<?php

/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/27/2016
 * Time: 8:04 AM
 */
class Avavadutaindonesia_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function detailRekan($data)
    {
        $param = array(
            'url_rekanan' => $data,
        );
        $datas = $this->db->select("SELECT * FROM `rekanan` WHERE url_rekanan=:url_rekanan LIMIT 0,1 ", $param);
        return json_encode($datas);
    }

    public function listRekan()
    {
        $datas = $this->db->select("SELECT * FROM `rekanan` ORDER BY id_rekanan ASC ");
        return json_encode($datas);
    }

}

?>