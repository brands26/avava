<?php

/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 23/11/2016
 * Time: 0:40
 */
class Setting_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getSetting($id = false)
    {
        $datas = $this->db->select('SELECT * FROM setting');
        $result = array();
        foreach ($datas as $data) {
            $result[$data['nama_setting']] = $data['value_setting'];
        }
        return json_encode($result);
    }
} ?>