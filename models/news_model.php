<?php

/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 22/11/2016
 * Time: 6:13
 */
class News_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function listNews($data)
    {
        $result = array();
        $param = array(
            'lang_post' => $data['lang_post'],
            'jenis_post' => $data['jenis_post'],
        );
        $count = $this->db->select("SELECT Count(*) as jumlah FROM `post` WHERE lang_post=:lang_post AND jenis_post=:jenis_post", $param);
        $datas = $this->db->select("SELECT * FROM `post` WHERE lang_post=:lang_post AND jenis_post=:jenis_post ORDER BY id_post DESC LIMIT " . (($data['page'] - 1) * 5) . ",5 ", $param);
        $result['search'] = false;
        $result['back'] = ($data['page'] > 1 ? true : false);
        $result['backpage'] = $data['page'] - 1;
        $result['next'] = ($count[0]['jumlah'] > 0 ? $data['page'] == ceil($count[0]['jumlah'] / 5) ? false : true : false);
        $result['nextpage'] = $data['page'] + 1;
        $result['data'] = $datas;
        return json_encode($result);
    }

    public function searchNews($data)
    {
        $result = array();
        $param = array(
            'lang_post' => $data['lang_post'],
            'jenis_post' => $data['jenis_post'],
        );
        $count = $this->db->select("SELECT Count(*) as jumlah FROM `post` WHERE lang_post=:lang_post AND jenis_post=:jenis_post AND (title_post LIKE '%" . $data['keyword'] . "%' OR content_post LIKE '%" . $data['keyword'] . "%')", $param);
        $datas = $this->db->select("SELECT * FROM `post` WHERE lang_post=:lang_post AND jenis_post=:jenis_post AND (title_post LIKE '%" . $data['keyword'] . "%' OR content_post LIKE '%" . $data['keyword'] . "%') ORDER BY id_post DESC LIMIT  " . (($data['page'] - 1) * 5) . ",5 ", $param);
        $result['search'] = true;
        $result['keyword'] = $data['keyword'];
        $result['back'] = ($data['page'] > 1 ? true : false);
        $result['backpage'] = $data['page'] - 1;
        $result['next'] = ($count[0]['jumlah'] > 0 ? $data['page'] == ceil($count[0]['jumlah'] / 5) ? false : true : false);
        $result['nextpage'] = $data['page'] + 1;
        $result['data'] = $datas;
        return json_encode($result);
    }

    public function recentNews($data)
    {
        $param = array(
            'lang_post' => $data['lang_post'],
            'jenis_post' => $data['jenis_post'],
        );
        $datas = $this->db->select("SELECT * FROM `post` WHERE lang_post=:lang_post AND jenis_post=:jenis_post ORDER BY id_post DESC LIMIT 8 ", $param);
        return json_encode($datas);
    }

    public function detailNews($data)
    {
        $param = array(
            'url_post' => $data,
        );
        $datas = $this->db->select("SELECT * FROM `post` WHERE url_post=:url_post LIMIT 0,1 ", $param);
        return json_encode($datas);
    }

    public function beforeAfterNews($data)
    {
        $param = array(
            'url_post' => $data,
        );
        $datas = $this->db->select("SELECT * FROM `post` WHERE  url_post=:url_post LIMIT 0,1 ", $param);
        $beforeAfter = array();
        $before = $this->db->select("SELECT * FROM `post` WHERE jenis_post=1 AND id_post not in(" . $datas[0]['id_post'] . ") ORDER BY RAND() LIMIT 0,1 ");
        if (!empty($before)) {
            $after = $this->db->select("SELECT * FROM `post` WHERE jenis_post=1 AND id_post not in(" . $datas[0]['id_post'] . ',' . $before[0]['id_post'] . ") ORDER BY RAND() LIMIT 0,1 ");
            $beforeAfter['data'][] = $before;
            $beforeAfter['data'][] = $after;
        }
        return json_encode($beforeAfter);
    }

    public function addComments($data)
    {
        $data = $this->db->insert('comments', $data);
        $data = array('id' => $this->db->lastInsertId());
        return json_encode($data);
    }

    public function comments($data)
    {
        $param = array(
            'id_post' => $data,
        );
        $datas = $this->db->select("SELECT * FROM `comments` WHERE id_post=:id_post ORDER BY created_comments DESC ", $param);
        return json_encode($datas);
    }

    public function listGroup()
    {
        $datas = $this->db->select("SELECT id_group,id_group_kategori,name_group,url_group FROM `group` order by id_group_kategori asc");
        $current_cat = 1;
        $result = array();
        $temp = array();
        for ($a = 0; $a < count($datas); $a++) {
            if ($datas[$a]["id_group_kategori"] != $current_cat) {
                $current_cat = $datas[$a]["id_group_kategori"];
                array_push($result, $temp);
                $temp = array();
            }
            $temp[] = $datas[$a];
            if ($a == count($datas) - 1) {
                array_push($result, $temp);
                $temp = array();
            }
        }
        return json_encode($result);
    }


}

?>