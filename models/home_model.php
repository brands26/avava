<?php

/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/19/2016
 * Time: 1:53 AM
 */
class Home_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }
    public function recentNews($data)
    {
        $param = array(
            'lang_post' => $data['lang_post'],
            'jenis_post' => $data['jenis_post'],
        );
        $datas = $this->db->select("SELECT * FROM `post` WHERE lang_post=:lang_post AND jenis_post=:jenis_post ORDER BY id_post DESC LIMIT 3 ", $param);
        return json_encode($datas);
    }
    public function listKategoriGroup()
    {
        $data = $this->db->select("SELECT * FROM `group_kategori` order by id_group_kategori asc ");
        return json_encode($data);
    }

    public function listGroup()
    {
        $datas = $this->db->select("SELECT id_group,id_group_kategori,name_group,url_group FROM `group` order by id_group_kategori asc");
        $current_cat=1;
        $result = array();
        $temp = array();
        for($a=0;$a<count($datas);$a++){
            if ($datas[$a]["id_group_kategori"] != $current_cat) {
                $current_cat =$datas[$a]["id_group_kategori"];
                array_push($result,$temp);
                $temp = array();
            }
            $temp[] = $datas[$a];
            if ($a == count($datas)-1){
                array_push($result,$temp);
                $temp = array();
            }
        }
        return json_encode($result);
    }


}

?>