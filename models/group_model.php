<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/19/2016
 * Time: 12:01 AM
 */
class Group_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function detailGroup($url)
    {
        $parameter = array();
        $parameter['url_group'] = $url;
        $data = $this->db->select("SELECT * FROM `group` WHERE url_group=:url_group  limit 0,1", $parameter);
        return json_encode($data);
    }



}

?>
