<?php

class Api_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function listProvinsi($id = false){
        if($id){
            $postData = array(
                'id_ref_provinsi' => $id
            );
            $data = $this->db->select('SELECT * FROM ref_provinsi WHERE id=:id', $postData);
            return json_encode($data);
        } else{

            $data = $this->db->select('SELECT * FROM ref_provinsi');
            return json_encode($data);
        }
    }
    public function listKota($parameter = false){
        if($parameter){
            $data = $this->db->select('SELECT * FROM ref_kota_kabupaten WHERE id_ref_kota_kabupaten=:id_ref_kota_kabupaten or id_ref_provinsi=:id_ref_provinsi', $parameter);
            return json_encode($data);
        } else{
            $data = $this->db->select('SELECT * FROM ref_kota_kabupaten');
            return json_encode($data);
        }
    }

    public function insert($data)
    {
        $this->db->insert('bank', $data);
        $data = array('nama_bank' => $data['nama_bank'], 'id' => $this->db->lastInsertId());
        echo json_encode($data);
    }
    public function insertProvisi($data)
    {
        $postData = array(
            'id_ref_provinsi' => $data['id'],
            'nama' => $data['nama']
        );
        $this->db->insert('ref_provinsi', $postData);
        $data = array('nama' => $data['nama'], 'id' => $this->db->lastInsertId());
        echo json_encode($data);
    }
}

?>