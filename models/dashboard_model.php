<?php

class Dashboard_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function login($data)
    {

        $sth = $this->db->prepare( "SELECT * FROM `users` WHERE username=:username AND password=:password");
        $sth->execute(array(
            ':username' => $data['username'],
            ':password' => md5($data['password']),
        ));

        $fetch = $sth->fetch();
        $count = $sth->rowCount();
        if ($count > 0) {
            Session::init();
            Session::set('loggedIn', true);
            header('location:' . URL .'id/dashboard');
        } else {
            header('location:'.URL.'id/dashboard/login#error');
        }
    }

    public function getSetting($id = false)
    {
        $datas = $this->db->select('SELECT * FROM setting');
        $result = array();
        foreach ($datas as $data) {
            $result[$data['nama_setting']] = $data['value_setting'];
        }
        return json_encode($result);
    }

    public function saveContact($data)
    {
        foreach ($data as $key => $value) {
            $data = array();
            $data["value_setting"] = $value;
            $where = "nama_setting = '" . $key . "'";
            $this->db->update('setting', $data, $where);
        }
        return true;
    }

    public function detailUser($id)
    {
        $parameter = array();
        $parameter['id_user'] = $id;
        $data = $this->db->select("SELECT * FROM `users` WHERE id_user=:id_user  limit 0,1", $parameter);
        return json_encode($data);
    }
    public function saveUser($data)
    {
        $result = $this->db->update('`users`',$data,' id_user=:id_user');
        return json_encode($result);
    }
    public function detailPage($id)
    {
        $parameter = array();
        $parameter['id_page'] = $id;
        $data = $this->db->select("SELECT * FROM `page` WHERE id_page=:id_page  limit 0,1", $parameter);
        return json_encode($data);
    }
    public function savePage($data)
    {
        $result = $this->db->update('`page`',$data,' id_page=:id_page');
        return json_encode($result);
    }


    public function listRekanan($data)
    {
        $result = $this->db->getDataTable(
            'rekanan',
            'id_rekanan',
            array(),
            array('id_rekanan','nama_rekanan', 'status_rekanan','web_rekanan','konten_rekanan','logo_rekanan','dibuat_rekanan'),
            $data);

        return $result;
    }
    public function detailRekanan($id)
    {
        $parameter = array();
        $parameter['id_rekanan'] = $id;
        $data = $this->db->select("SELECT * FROM `rekanan` WHERE id_rekanan=:id_rekanan  limit 0,1", $parameter);
        return json_encode($data);
    }
    public function saveRekanan($data)
    {
        $this->db->insert('rekanan', $data);
        $data = array('id' => $this->db->lastInsertId());
        return json_encode($data);
    }
    public function updateRekanan($data)
    {
        $result = $this->db->update('`rekanan`',$data,' id_rekanan=:id_rekanan');
        return json_encode($result);
    }
    public function deleteRekanan($data)
    {
        $result = $this->db->delete('`rekanan`',' id_rekanan='.$data['id_rekanan']);
        return json_encode($result);
    }

    public function listComments($data)
    {
        $result = $this->db->getDataTable(
            'comments',
            'id_comments',
            array('comments'=>'post'),
            array('id_comments','title_post', 'name_comments','email_comments','content_comments','created_comments'),
            $data);

        return $result;
    }
    public function deleteComments($data)
    {
        $result = $this->db->delete('`comments`',' id_comments='.$data['id_comments']);
        return json_encode($result);
    }



    public function savePost($data)
    {
        $this->db->insert('post', $data);
        $data = array('nama' => $data['nama'], 'id' => $this->db->lastInsertId());
        return json_encode($data);
    }
    public function updatePost($data)
    {
        $result = $this->db->update('`post`',$data,' id_post=:id_post');
        return json_encode($result);
    }
    public function deletePost($data)
    {
        $result = $this->db->delete('`post`',' id_post='.$data['id_post']);
        return json_encode($result);
    }

    public function detailGroup($id)
    {
        $parameter = array();
        $parameter['id_group'] = $id;
        $data = $this->db->select("SELECT * FROM `group` WHERE id_group=:id_group  limit 0,1", $parameter);
        return json_encode($data);
    }
    public function saveGroup($data)
    {
        $this->db->insert('`group`', $data);
        $data = array('id' => $this->db->lastInsertId());
        return json_encode($data);
    }
    public function updateGroup($data)
    {
        $result = $this->db->update('`group`',$data,' id_group=:id_group');
        return json_encode($result);
    }
    public function deleteGroup($data)
    {
        $result = $this->db->delete('`group`',' id_group='.$data['id_group']);
        return json_encode($result);
    }

    public function listPost($jenis, $lang = false, $page = 0)
    {
        if($page != 0){
            $page-=1;
        }
        $page = $page * 5;
        $limit = " limit $page,5";
        if(!$lang){
            $lang = "";
        }
        $parameter = array();
        $parameter['jenis_post'] = $jenis;
        $parameter['lang_post'] = $lang;
        $data = $this->db->select("SELECT * FROM `post` WHERE jenis_post=:jenis_post and lang_post=:lang_post $limit", $parameter);
        return json_encode($data);
    }
    public function listGroup($data)
    {
        //print_r($data);
        $result = $this->db->getDataTable(
            'group',
            'id_group',
            array('group'=>'group_kategori'),
            array('id_group','name_group', 'name_group_kategori','content_group'),
            $data);
        return $result;
    }
    public function listKategoriGroup()
    {
        $data = $this->db->select("SELECT * FROM `group_kategori` order by id_group_kategori asc ");
        return json_encode($data);
    }
    public function listGroupKategori($data)
    {
        //print_r($data);
        $result = $this->db->getDataTable(
            'group_kategori',
            'id_group_kategori',array(),
            array('id_group_kategori','name_group_kategori'),
            $data);
        return $result;
    }
    public function detailGroupCategory($id)
    {
        $parameter = array();
        $parameter['id_group_kategori'] = $id;
        $data = $this->db->select("SELECT * FROM `group_kategori` WHERE id_group_kategori=:id_group_kategori  limit 0,1", $parameter);
        return json_encode($data);
    }
    public function updateGroupCategory($data)
    {
        $result = $this->db->update('`group_kategori`',$data,' id_group_kategori=:id_group_kategori');
        return json_encode($result);
    }
    public function saveGroupCategory($data)
    {
        $this->db->insert('`group_kategori`', $data);
        $data = array('id' => $this->db->lastInsertId());
        return json_encode($data);
    }
    public function deleteGroupCategory($data)
    {
        $result = $this->db->delete('`group_kategori`',' id_group_kategori='.$data['id_group_kategori']);
        return json_encode($result);
    }
    public function listNews($data)
    {
        //print_r($data);
        $result = $this->db->getDataTable(
            'post',
            'id_post',
            array(),
            array('id_post','title_post','content_post','image_post','created_post'),
            $data,
            array('jenis_post'=>1)
        );
        return $result;
    }
    public function listEvent($data)
    {
        //print_r($data);
        $result = $this->db->getDataTable(
            'post',
            'id_post',
            array(),
            array('id_post','title_post','content_post','image_post','created_post'),
            $data,
            array('jenis_post'=>2)
        );
        return $result;
    }
    public function detailPost($id)
    {
        $parameter = array();
        $parameter['id_post'] = $id;
        $data = $this->db->select("SELECT * FROM `post` WHERE id_post=:id_post  limit 0,1", $parameter);
        return json_encode($data);
    }
    public function countPost($jenis, $lang = false)
    {
        if(!$lang){
            $lang = "";
        }
        $parameter = array();
        $parameter['jenis_post'] = $jenis;
        $parameter['lang_post'] = $lang;
        $data = $this->db->select("SELECT Count(*) as jumlah FROM `post` WHERE jenis_post=:jenis_post and lang_post=:lang_post", $parameter);
        return json_encode($data);
    }

}

?>