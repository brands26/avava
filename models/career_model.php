<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 22/11/2016
 * Time: 6:02
 */
class Career_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function detailPage($id)
    {
        $parameter = array();
        $parameter['id_page'] = $id;
        $data = $this->db->select("SELECT * FROM `page` WHERE id_page=:id_page  limit 0,1", $parameter);
        return json_encode($data);
    }


}
?>