
$(document).on('click', '.btn-batal-produk', function () {
    var index = $(this).attr('data-id');
    var cart = null;
    if (localStorage.getItem('cart') == null)
        cart = [];
    else {
        cart = JSON.parse(localStorage.cart);
        cart.splice(index, 1);
    }
    localStorage.setItem('cart', JSON.stringify(cart));
    loadCart();
});
function loadCart() {

    if (localStorage.cart.length != 0) {
        var cart = JSON.parse(localStorage.cart);
        var jumlahItem = cart.length;
        var totalBiaya = 0;
        if (jumlahItem != 0) {
            $('ul#cart .body-cart').empty();
            $('.cart-badge').css('display', 'block').html(cart.length);
            for (var a = 0; a < jumlahItem; a++) {
                totalBiaya += cart[a].totalHarga;
                $('ul#cart .body-cart').append(
                    '<li class="produk">' +
                    '<div class="row">' +
                    '<div class="col s4">' +
                    '<img src="' + URL + 'public/images/produk/' + cart[a].foto + '.jpg" alt="" class="circle responsive-img">' +
                    ' </div>' +
                    '<div class="detail-produk  col s8">' +
                    '<span class="nama-produk">' +
                    cart[a].nama +
                    '</span>' +
                    '<span class="jumlah-produk right-align">' +
                    cart[a].jumlah +
                    '</span>' +
                    '<span class="Total-produk right-align">Rp.' +
                    cart[a].totalHarga +
                    '</span>' +
                    '</div>' +
                    '<div class="batal-produk col s12">' +
                    '<a data-id="' + a + '" class="btn-batal-produk waves-effect waves-light btn">batalkan</a>' +
                    '</div>' +
                    '</div>' +
                    '</li>');

            }
            $('#cart .total-produk').html('Rp.' + totalBiaya);
        }
        else {
            var cart = [];
            $('ul#cart .body-cart').empty();
            $('#cart .total-produk').html('Rp.' + 0);
            $('.cart-badge').css('display', 'none');
        }
    }

    else {
        $('.cart-badge').css('display', 'none');
    }
}