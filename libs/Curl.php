<?php

class Curl{

    private $_ch=null;
    private $_fields=null;
    private $_fields_string=null;

    public function __construct(){
        $this->_ch = curl_init();
    }
    public function setField($fields = array()){
        $this->_fields = $fields;
        foreach($fields as $key=>$value) { $this->_fields_string .= $key.'='.$value.'&'; }
        rtrim($this->_fields_string,'&');

    }
    public function setUrl($url){
        curl_setopt($this->_ch,CURLOPT_URL, $url);
        curl_setopt($this->_ch,CURLOPT_POST, count($this->_fields));
        curl_setopt($this->_ch,CURLOPT_POSTFIELDS, $this->_fields_string);
        //curl_setopt($this->_ch, CURLOPT_FOLLOWLOCATION , true);
    }
    public function run(){
        $result = curl_exec($this->_ch);
        return $result;
    }
    public function close(){
        curl_close($this->_ch);
    }
}
?>