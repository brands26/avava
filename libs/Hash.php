<?php

class Hash{


    /**
     * @param string $algo (md5,sha1,whirpool,etc)
     * @param string $data
     * @param string $salt
     * @return string hashed data.
     */
    public static function create($algo, $data, $salt){

        $context = hash_init($algo, HASH_HMAC, $salt);
        hash_update($context,$data);

        return hash_final($context);
    }
}


?>