<?php
require 'models/setting_model.php';

class Controller{

    private $_language = null;
    private $_model = null;

    function __construct(){
        Session::init();
        $this->view = new View();
    }

    public function loadModel($name){
        $path = 'models/'.$name.'_model.php';
        if(file_exists($path)){
            require 'models/'.$name.'_model.php';

            $modelName = $name . '_Model';
            $this->model = new $modelName;
        }
    }
    public function loadLanguage($name){
        $path = 'languanges/'.$name.'.php';
        if(file_exists($path)){
            require 'languanges/'.$name.'.php';
            $this->_language = new $name;
            $this->view->menus = $this->_language->getMenu();
            $this->_model = new Setting_Model();
            $this->view->setting = json_decode($this->_model->getSetting());
        }
    }
    public function showError(){
        require 'controllers/error.php';
        $page = new Error();
        $page->view->menus = $this->_language->getMenu();
        $page->index();
    }
    public function getLanguage(){
        $this->view->menus = $this->_language;
    }

}




?>