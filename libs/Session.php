<?php

class Session{

    public static function init(){
        @session_start();
    }
    public static function set($key, $value=false){
        if(is_array($key)){
            foreach ($key as $keyArray=>$valueArray){
                $_SESSION[$keyArray] = $valueArray;
            }
        }
        else{
            $_SESSION[$key] = $value;
        }
    }
    public static function get($key){
        if(isset($_SESSION[$key]))
            return $_SESSION[$key];
    }
    public static function un_Set($key){
        if(is_array($key)){
            foreach ($key as $keyArray=>$valueArray){
                unset($_SESSION[$valueArray]);
            }
        }
        else{
            unset($_SESSION[$key]);
        }
    }
    public static function destroy(){
        session_destroy();
    }
}
?>