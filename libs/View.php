<?php

class View
{

    private $_logged = false;
    private $_role = false;
    private $title = null;

    function __construct()
    {
        $this->_logged = Session::get('loggedIn');
        if($this->_logged){
            $this->_role=Session::get('role');
        }
    }

    public function setHeader($header = false)
    {
        if ($header) {
            switch ($header){
                case 'dashboard':
                    require 'views/header/dashboard-new.php';
                    break;
                case 'dashboard-no':
                    require 'views/header/dashboard-no-header.php';
                    break;
                default:
                    require 'views/header/header.php';
                    break;
            }

        } else {
            require 'views/header/no-header.php';
        }
    }
    public function setSidebar()
    {
        require 'views/header/sidebar-new.php';
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function setBody($body)
    {
        require 'views/' . $body . '.php';
    }

    public function setFooter($footer = false)
    {
        if ($footer) {
            switch ($footer){
                case 'dashboard':
                    require 'views/footer/dashboard-footer-new.php';
                    break;
                default:
                    require 'views/footer/footer.php';
                    break;
            }
        } else {
            require 'views/footer/no-footer.php';
        }
    }

    public function render($header = false, $body, $footer = false, $noInclude = false)
    {
        /*
        if ($noInclude == true) {
            require 'views/header/no-header.php';
            require 'views/' . $name . '.php';
            require 'views/footer/no-footer.php';
        } else {
            if ($logged == false) {
                require 'views/header/header.php';
            } else {
                if (Session::get('role') === 'owner') {
                    require 'views/header/dashboard-owner.php';
                } else {
                    require 'views/header/header-user.php';
                }
            }
            require 'views/' . $name . '.php';
            require 'views/footer/footer.php';
        }
        */
    }

}


?>