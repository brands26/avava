<?php

class Database extends PDO
{

    private $db;

    public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
    {
        parent::__construct($DB_TYPE . ':host=' . $DB_HOST . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASS);

        //SHOW ERROR
        parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }


    /**
     * @param String $sql
     * @param Array $array parameter
     * @param Constant $fetchMode
     * @return mixed
     */
    public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        $sth->execute();
        return $sth->fetchAll($fetchMode);

    }

    /**
     * insert
     * @param String $table
     * @param String $data in Array
     */
    public function insert($table, $data = array())
    {

        ksort($data);
        $name = implode('`,`', array_keys($data));
        $value = ':' . implode(',:', array_keys($data));

        $sth = $this->prepare("INSERT INTO $table (`$name`) VALUES ($value)");
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        $sth->execute();
    }

    /**
     * update
     * @param String $table
     * @param String $data in Array
     * @param String $where query WHERE
     * @return mixed
     */
    public function update($table, $data, $where)
    {
        ksort($data);
        $detail = null;
        foreach ($data as $key => $value) {
            $detail .= "`$key`=:$key,";
        }
        $detail = rtrim($detail, ',');
        $sth = $this->prepare("UPDATE $table SET $detail WHERE $where");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        return $sth->execute();
    }

    public function delete($table, $where, $limit = 1)
    {
        return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
    }

    public function getDataTable($table, $index_column, $join_table = array(), $columns, $GET, $where = array())
    {
        /*
         * Script:    DataTables server-side script for PHP and MySQL
         * Copyright: 2012 - John Becker, Beckersoft, Inc.
         * Copyright: 2010 - Allan Jardine
         * License:   GPL v2 or BSD (3-point)
         */
        $sJOin = '';
        if (count($join_table) > 0) {
            foreach ($join_table as $key => $value) {
                $sJOin = 'LEFT JOIN `' . $value . '` ON ' . $key . '.id_' . $value . '=' . $value . '.id_' . $value;
            }
        }
        // Paging
        $sLimit = "";
        //echo (isset($GET['start'])?'true':'false');
        if (isset($GET['start']) && $GET['length'] != '-1') {
            $sLimit = "LIMIT " . intval($GET['start']) . ", " . intval($GET['length']);
        }

        // Ordering
        $sOrder = "ORDER BY  ";
        for ($i = 0; $i < count($GET['order']); $i++) {
            if ($GET['columns'][$GET['order'][$i]['column']]['orderable'] == "true") {
                $sortDir = $GET['order'][$i]['dir'];
                $sOrder .= "`" . $GET['columns'][$GET['order'][$i]['column']]['data'] . "` " . $sortDir . ", ";
            }
        }

        $sOrder = substr_replace($sOrder, "", -2);

        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "";
        //echo (isset($GET['searchable'])?'true':'false');
        if (isset($GET['search']) && $GET['search'] != "") {
            $sWhere = "WHERE (";
            for ($i = 0; $i < count($GET['columns']); $i++) {
                if (isset($GET['columns'][$i]['data'])
                    && $GET['columns'][$i]['data'] != ''
                    && isset($GET['columns'][$i]['searchable'])
                    && $GET['columns'][$i]['searchable'] == "true"
                ) {
                    $sWhere .= "`" . $GET['columns'][$i]['data'] . "` LIKE :search OR ";
                }
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        if (count($where)!= 0) {
            if ($sWhere == '') {
                $sWhere .= ' Where (';
            } else{
                $sWhere .=' AND (';
            }
            foreach ($where as $key=>$value){
                $sWhere.= " $key LIKE $value OR";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .=')';
        }


        /*
        // Individual column filtering
        for ($i = 0; $i < count($columns); $i++) {
            if (isset($GET['columns'][$i]) && $GET['columns'][$i]['searchable'] == "true" && $GET['columns'][$i]['search'] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $columns[$i] . "` LIKE :search" . $i . " ";
            }
        }
        */
        //echo $sWhere;

        // SQL queries get data to display
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS `" . str_replace(" , ", " ", implode("`, `", $columns)) . "` FROM `" . $table . "` $sJOin " . $sWhere . " " . $sOrder . " " . $sLimit;
        $statement = $this->prepare($sQuery);
        //echo $sQuery;
        // Bind parameters
        if (isset($GET['search']) && $GET['search'] != "") {
            $statement->bindValue(':search', '%' . $GET['search']['value'] . '%', PDO::PARAM_STR);
        }
        /*
        for ($i = 0; $i < count($columns); $i++) {
            if (isset($GET['columns'][$i]['searchable']) && $GET['columns'][$i]['searchable'] == "true" && $GET['columns'][$i]['search'] != '') {
                $statement->bindValue(':search' . $i, '%' . $GET['search']['value'] . '%', PDO::PARAM_STR);
            }
        }
        */
        //echo $sQuery;
        $statement->execute();
        $rResult = $statement->fetchAll();

        $iFilteredTotal = current($this->query('SELECT FOUND_ROWS()')->fetch());

        // Get total number of rows in table
        $sQuery = "SELECT COUNT(`" . $index_column . "`) FROM `" . $table . "`";
        $iTotal = current($this->query($sQuery)->fetch());

        // Output
        $output = array(
            "draw" => intval((isset($GET['draw']) ? $GET['draw'] : '1')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        // Return array of values
        foreach ($rResult as $aRow) {
            $row = array();
            for ($i = 0; $i < count($columns); $i++) {
                if ($columns[$i] == "version") {
                    // Special output formatting for 'version' column
                    $row[] = ($aRow[$columns[$i]] == "0") ? '-' : $aRow[$columns[$i]];
                } else if ($columns[$i] != ' ') {
                    $row[$columns[$i]] = strip_tags($aRow[$columns[$i]], '');
                }
                if ($columns[$i] == 'content_group') {
                    $row[$columns[$i]] = substr(strip_tags($aRow[$columns[$i]], ''), 0, 100);
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }


}

?>