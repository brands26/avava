<?php
date_default_timezone_set ('Asia/Jakarta');
class Neko
{

    private $_url = null;
    private $_controller = null;

    function __construct()
    {
        $this->_getUrl();
        if (empty($this->_url[0]) || empty($this->_url[1])) {
            $this->_loadDefaultController();
            return false;
        }
        $this->_loadExitingController();
        $this->_callControllerMethod();


    }

    public function _getUrl()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
    }
    public function _loadDefaultController()
    {
        require 'controllers/index.php';
        $this->_controller = new Index();
        $this->_controller->index();

    }

    public function _loadExitingController()
    {
        $file = 'controllers/' . $this->_url[1] . '.php';
        if (file_exists($file)) {
            require $file;
            $this->_controller = new $this->_url[1];
            $this->_controller->loadModel($this->_url[1]);
            $this->_controller->loadLanguage($this->_url[0]);

        } else {
            $this->error();
            exit;
        }

    }

    public function _callControllerMethod()
    {
        $lenght = count($this->_url);
        if ($lenght > 2) {
            if (!method_exists($this->_controller, $this->_url[2])){
                $this->_controller->showError();
            }
        }
        switch ($lenght) {
            case 6:
                $this->_controller->{$this->_url[2]}($this->_url[3], $this->_url[4], $this->_url[5]);
                break;
            case 5:
                $this->_controller->{$this->_url[2]}($this->_url[3], $this->_url[4]);
                break;
            case 4:
                $this->_controller->{$this->_url[2]}($this->_url[3]);
                break;
            case 3:
                $this->_controller->{$this->_url[2]}();
                break;
            default :
                $this->_controller->index();
                break;
        }

    }


    private function error()
    {
        require 'controllers/error.php';
        $this->_controller = new Error();
        $this->_controller->loadLanguage($this->_url[0]);
        $this->_controller->index();
        return false;
    }
}

?>