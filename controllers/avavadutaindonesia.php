<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 24/11/2016
 * Time: 20:14
 */

class Avavadutaindonesia extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        $this->view->rekanan = json_decode($this->model->listRekan());
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js');
        $this->view->setBody('duta/index');
        $this->view->setFooter('default');
    }
    function struktur(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/struktur');
        $this->view->setFooter('default');
    }
    function operasional(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/operasional');
        $this->view->setFooter('default');
    }
    function sistem(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/sistem');
        $this->view->setFooter('default');
    }
    function unsur(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/unsur');
        $this->view->setFooter('default');
    }
    function assets(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/assets');
        $this->view->setFooter('default');
    }
    function referensi($id = false){
        switch ($id){
            case 1:
                $this->view->setHeader('default');
                $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
                $this->view->setBody('duta/referensi1');
                $this->view->setFooter('default');
                break;
            case 2:
                $this->view->setHeader('default');
                $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
                $this->view->setBody('duta/referensi2');
                $this->view->setFooter('default');
                break;
            case 3:
                $this->view->setHeader('default');
                $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
                $this->view->setBody('duta/referensi3');
                $this->view->setFooter('default');
                break;
            case 4:
                $this->view->setHeader('default');
                $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
                $this->view->setBody('duta/referensi4');
                $this->view->setFooter('default');
                break;
            default:
                $this->showError();
        }

    }
    function legalitas(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/legalitas');
        $this->view->setFooter('default');
    }
    function dokumen(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/dokumen');
        $this->view->setFooter('default');
    }
    function prestasi(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/prestasi');
        $this->view->setFooter('default');
    }
    function aktifitas(){
        $this->view->setHeader('default');
        $this->view->js = array('duta/js/index.js','duta/js/scoll-sidebar.js');
        $this->view->setBody('duta/aktifitas');
        $this->view->setFooter('default');
    }
    function rekan($url = false){
        if(!$url){
            $this->showError();
        }else{
            $this->view->rekan  = json_decode($this->model->detailRekan($url));
            $this->view->setHeader('default');
            $this->view->setBody('duta/rekan');
            $this->view->setFooter('default');
        }
    }

}
?>