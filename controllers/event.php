<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 17/11/2016
 * Time: 8:39
 */
class Event extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        $data = array();
        $data['page'] = 1;
        $data['lang_post'] = ($this->view->menus['lang'] == 'id'? 1:2);
        $data['jenis_post'] = 2;
        $this->view->Event  = json_decode($this->model->listEvent($data));
        $this->view->recentEvent  = json_decode($this->model->recentEvent($data));
        $this->view->setHeader('default');
        $this->view->js = array('Event/js/index.js');
        $this->view->setBody('event/index');
        $this->view->setFooter('default');
    }

    function post($url){
        $this->view->event  = json_decode($this->model->detailEvent($url));
        $this->view->beforeafterEvent  = json_decode($this->model->beforeAfterEvent($url));
        $this->view->comments  = json_decode($this->model->comments($this->view->event[0]->id_post));
        $this->view->setHeader('default');
        $this->view->js = array('Event/js/index.js');
        $this->view->setBody('event/post');
        $this->view->setFooter('default');
    }
    function page($page){
        $data = array();
        $data['page'] = $page;
        $data['lang_post'] = ($this->view->menus['lang'] == 'id'? 1:2);
        $data['jenis_post'] = 2;
        $this->view->Event  = json_decode($this->model->listEvent($data));
        $this->view->recentEvent  = json_decode($this->model->recentEvent($data));
        $this->view->setHeader('default');
        $this->view->js = array('Event/js/index.js');
        $this->view->setBody('event/index');
        $this->view->setFooter('default');
    }
    function search($keyword='',$page=1){
        $data = array();
        $data['keyword'] = $keyword;
        $data['page'] = $page;
        $data['lang_post'] = ($this->view->menus['lang'] == 'id'? 1:2);
        $data['jenis_post'] = 2;
        $this->view->Event  = json_decode($this->model->searchEvent($data));
        $this->view->recentEvent  = json_decode($this->model->recentEvent($data));
        $this->view->setHeader('default');
        $this->view->js = array('Event/js/index.js');
        $this->view->setBody('event/index');
        $this->view->setFooter('default');
    }
    function comment($id){
        $data = array();
        $data['id_post'] = $id;
        $data['name_comments'] = $_POST['name_comments'];
        $data['email_comments'] = $_POST['email_comments'];
        $data['content_comments'] = $_POST['content_comments'];
        $data['created_comments'] = date("Y-m-d H:i:s");
        if(json_decode($this->model->addComments($data))){
            header('location:'.URL.$this->view->menus['lang'].'/news/event/'.$_POST['url']);
        }
    }
}


?>