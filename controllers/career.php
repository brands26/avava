<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 17/11/2016
 * Time: 8:50
 */
class Career extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        $this->view->page = json_decode($this->model->detailPage(2));
        $this->view->setHeader('default');
        $this->view->js = array('career/js/index.js');
        $this->view->setBody('career/index');
        $this->view->setFooter('default');
    }
}


