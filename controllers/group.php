<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/18/2016
 * Time: 10:23 PM
 */
class Group extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        header('location:'.URL.$this->view->menus['lang'].'/'.'home');
        exit;
    }
    function detail($url){
        $this->view->group = json_decode($this->model->detailGroup($url));
        $this->view->setHeader('default');
        $this->view->js = array('group/js/index.js');
        $this->view->setBody('group/index');
        $this->view->setFooter('default');
    }
    function generalcarbonindustry(){

        $this->view->setHeader('default');
        $this->view->js = array('group/js/index.js');
        $this->view->setBody('group/generalcarbonindustry');
        $this->view->setFooter('default');
    }
}



?>