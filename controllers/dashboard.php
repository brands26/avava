<?php

/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 18/11/2016
 * Time: 1:32
 */
class Dashboard extends Controller
{

    function __construct()
    {
        parent::__construct();
        Session::init();
        $this->view->element = array('');

        $this->view->cssDefault = array(
            'bootstrap/css/bootstrap.css',
            'dropzone/dropzone.css',
            'node-waves/waves.css',
            'animate-css/animate.css',
            'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css',
            'bootstrap-select/css/bootstrap-select.css'
        );
        $this->view->jsDefault = array(
            'jquery/jquery.min.js',
            'bootstrap/js/bootstrap.js',
            'bootstrap-select/js/bootstrap-select.js',
            'jquery-slimscroll/jquery.slimscroll.js',
            'node-waves/waves.js'
        );
    }

    function index()
    {
        $this->geLoged();
        $this->view->setHeader('dashboard');
        $this->view->setSidebar();
        $this->view->setBody('dashboard/index');
        $this->view->setFooter('dashboard');
    }

    function blank()
    {
        $this->geLoged();
        $this->showError();
    }

    function login()
    {

        $this->view->setHeader('dashboard-no');
        $this->view->js = array('dashboard/js/login.js');
        $this->view->setBody('dashboard/login');
        $this->view->setFooter('dashboard');
    }

    function setting($parameter = false)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "update";
                    $data = array(
                        'head_office' => $_POST['head_office'],
                        'telp' => $_POST['telp'],
                        'email' => $_POST['email'],
                        'facebook' => $_POST['facebook'],
                        'twitter' => $_POST['twitter'],
                        'Linkedin' => $_POST['Linkedin'],
                        'Instagram' => $_POST['Instagram'],
                    );
                    if ($this->model->saveContact($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/setting');
                        exit;
                    }
                    break;
                default:
                    break;
            }
        } else {
            $this->view->setting = json_decode($this->model->getSetting());
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
            $this->view->js = array('dashboard/js/setting.js');
            $this->view->setBody('dashboard/formsetting');
            $this->view->setFooter('dashboard');
        }
    }
    function account($parameter = false)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "update";
                    if($_POST['password']!=null){
                        $data = array(
                            'id_user' => $_POST['id_user'],
                            'username' => $_POST['username'],
                            'password' => md5($_POST['password']),
                        );
                    }else{
                        $data = array(
                            'id_user' => $_POST['id_user'],
                            'username' => $_POST['username']
                        );
                    }
                    if ($this->model->saveUser($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/account');
                        exit;
                    }
                    break;
                default:
                    break;
            }
        } else {
            $this->view->setting = json_decode($this->model->detailUser(1));
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
            $this->view->js = array('dashboard/js/setting.js');
            $this->view->setBody('dashboard/formaccount');
            $this->view->setFooter('dashboard');
        }
    }

    function contact($parameter = false)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "save";
                    $data = array(
                        'head_office' => $_POST['head_office'],
                        'telp' => $_POST['telp'],
                        'email' => $_POST['email']
                    );
                    if ($this->model->saveContact($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/');
                        exit;
                    }
                    break;
                default:
                    break;
            }
        } else {
            $this->view->setting = json_decode($this->model->getSetting());
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->js = array('dashboard/js/contact.js');
            $this->view->setBody('dashboard/contact');
            $this->view->setFooter('dashboard');
        }
    }

    function page($parameter = false,$id = false)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "save";
                    $data = array(
                        'id_page' => $_POST['id_page'],
                        'title_page' => $_POST['title_page'],
                        'title_page_en' => $_POST['title_page_en'],
                        'image_page' => $_POST['image_page'],
                        'content_page' => $_POST['content_page'],
                        'content_page_en' => $_POST['content_page_en'],
                    );
                    if ($this->model->savePage($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard');
                        exit;
                    }
                    break;
                case "edit";
                    $this->view->page = json_decode($this->model->detailPage($id));
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/page.js');
                    $this->view->setBody('dashboard/formpage');
                    $this->view->setFooter('dashboard');
                    break;
                default:
                    $this->showError();
                    break;
            }
        } else {
            $this->showError();
        }
    }

    function group($parameter = false, $lang = false, $page = 0)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "save";
                    $data = array(
                        'id_group_kategori' => $_POST['id_group_kategori'],
                        'name_group' => $_POST['name_group'],
                        'name_group_en' => $_POST['name_group'],
                        'url_group' => preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $this->limit_words($_POST['name_group'], 6))),
                        'content_group' => $_POST['content_group'],
                        'content_group_en' => $_POST['content_group_en'],
                        'lang_group' => $_POST['lang_group'],
                        'caraousel_image_group' =>($_POST['caraousel_image_group']!=null?$_POST['caraousel_image_group']:""),

                    );
                    if ($this->model->saveGroup($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/group#success');
                        exit;
                    }
                    break;
                case "delete";
                    $data = array(
                        'id_group' => $lang
                    );
                    if ($this->model->deleteGroup($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/group#success');
                        exit;
                    }
                    break;
                case "update";
                    $data = array(
                        'id_group' => $_POST['id_group'],
                        'id_group_kategori' => $_POST['id_group_kategori'],
                        'name_group' => $_POST['name_group'],
                        'name_group_en' => $_POST['name_group'],
                        'url_group' => preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $this->limit_words($_POST['name_group'], 6))),
                        'content_group' => $_POST['content_group'],
                        'content_group_en' => $_POST['content_group_en'],
                        'lang_group' => $_POST['lang_group'],
                        'caraousel_image_group' => ($_POST['caraousel_image_group']!=null?$_POST['caraousel_image_group']:""),

                    );
                    if ($this->model->updateGroup($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/group#success');
                        exit;
                    }
                    break;
                case "edit";
                    $this->view->kategoriGroup = json_decode($this->model->listKategoriGroup());
                    $this->view->group = json_decode($this->model->detailGroup($lang));
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/group.js');
                    $this->view->setBody('dashboard/formgroup');
                    $this->view->setFooter('dashboard');
                    break;
                case "add";

                    $this->view->kategoriGroup = json_decode($this->model->listKategoriGroup());
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/group.js');
                    $this->view->setBody('dashboard/formgroup');
                    $this->view->setFooter('dashboard');
                    break;
                default:
                    $this->showError();
                    break;
            }
        } else {
            //$this->view->listGroup = json_decode($this->model->listGroup());
            $this->view->jsPlugin = array(
                'jquery-datatable/jquery.dataTables.js',
                'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');
            $this->view->js = array(
                'dashboard/js/jquery-datatable.js'
            );
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->setBody('dashboard/group');
            $this->view->setFooter('dashboard');
        }
    }

    function groupcategory($parameter = false, $lang = false, $page = 0)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "save";
                    $data = array(
                        'name_group_kategori' => $_POST['name_group_kategori'],
                    );
                    if ($this->model->saveGroupCategory($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/groupcategory#success');
                        exit;
                    }
                    break;
                case "delete";
                    $data = array(
                        'id_group_kategori' => $lang
                    );
                    if ($this->model->deleteGroupCategory($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/groupcategory#success');
                        exit;
                    }
                    break;
                case "update";
                    $data = array(
                        'id_group_kategori' => $_POST['id_group_kategori'],
                        'name_group_kategori' => $_POST['name_group_kategori'],
                    );
                    if ($this->model->updateGroupCategory($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/groupcategory#success');
                        exit;
                    }
                    break;
                case "edit";
                    $this->view->post = json_decode($this->model->detailGroupCategory($lang));
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->js = array('dashboard/js/post.js');
                    $this->view->setBody('dashboard/formgroupkategori');
                    $this->view->setFooter('dashboard');
                    break;
                case "add";
                    $this->view->setting = json_decode($this->model->getSetting());
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/event.js');
                    $this->view->setBody('dashboard/formgroupkategori');
                    $this->view->setFooter('dashboard');
                    break;
                default:
                    $this->showError();
                    break;
            }
        } else {
            $this->view->jsPlugin = array(
                'jquery-datatable/jquery.dataTables.js',
                'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');
            $this->view->js = array(
                'dashboard/js/groupkategori.js'
            );
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->setBody('dashboard/groupkategori');
            $this->view->setFooter('dashboard');
        }
    }

    function news($parameter = false, $lang = false, $page = 0)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "save";
                    $data = array(
                        'title_post' => $_POST['title_post'],
                        'url_post' => preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $this->limit_words($_POST['title_post'], 6))),
                        'content_post' => $_POST['content_post'],
                        'jenis_post' => 1,
                        'lang_post' => $_POST['lang_post'],
                        'image_post' => ($_POST['image_post']!=null?$_POST['image_post']:URL."public/images/961s3.jpg"),
                        'created_post' => date("Y-m-d"),

                    );
                    if ($this->model->savePost($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/news#success');
                        exit;
                    }
                    break;
                case "delete";
                    $data = array(
                        'id_post' => $lang
                    );
                    if ($this->model->deletePost($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/news#success');
                        exit;
                    }
                    break;
                case "update";
                    $data = array(
                        'id_post' => $_POST['id_post'],
                        'title_post' => $_POST['title_post'],
                        'url_post' => preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $this->limit_words($_POST['title_post'], 6))),
                        'content_post' => $_POST['content_post'],
                        'jenis_post' => 1,
                        'lang_post' => $_POST['lang_post'],
                        'image_post' => ($_POST['image_post']!=null?$_POST['image_post']:URL."public/images/961s3.jpg"),
                        'created_post' => date("Y-m-d"),

                    );
                    if ($this->model->updatePost($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/news#success');
                        exit;
                    }
                    break;
                case "edit";
                    $this->view->post = json_decode($this->model->detailPost($lang));
                    $this->view->setting = json_decode($this->model->getSetting());
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/post.js');
                    $this->view->setBody('dashboard/formpost');
                    $this->view->setFooter('dashboard');
                    break;
                case "add";
                    $this->view->setting = json_decode($this->model->getSetting());
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/post.js');
                    $this->view->setBody('dashboard/formpost');
                    $this->view->setFooter('dashboard');
                    break;
                default:
                    $this->showError();
                    break;
            }
        } else {
            $this->view->jsPlugin = array(
                'jquery-datatable/jquery.dataTables.js',
                'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');
            $this->view->js = array(
                'dashboard/js/news.js'
            );
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->js = array('dashboard/js/news.js');
            $this->view->setBody('dashboard/news');
            $this->view->setFooter('dashboard');
        }
    }
    function rekanan($parameter = false, $lang = false, $page = 0)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "save";
                    $data = array(
                        'nama_rekanan' => $_POST['nama_rekanan'],
                        'url_rekanan' => preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $this->limit_words($_POST['nama_rekanan'], 6))),
                        'web_rekanan' => $_POST['web_rekanan'],
                        'konten_rekanan' => $_POST['konten_rekanan'],
                        'konten_en_rekanan' => $_POST['konten_en_rekanan'],
                        'status_rekanan' => $_POST['status_rekanan'],
                        'logo_rekanan' => ($_POST['logo_rekanan']!=null?$_POST['logo_rekanan']:''),
                        'dibuat_rekanan' => date("Y-m-d"),

                    );
                    if ($this->model->saveRekanan($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/rekanan#success');
                        exit;
                    }
                    break;
                case "delete";
                    $data = array(
                        'id_rekanan' => $lang
                    );
                    if ($this->model->deleteRekanan($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/rekanan#success');
                        exit;
                    }
                    break;
                case "update";
                    $data = array(
                        'id_rekanan' => $_POST['id_rekanan'],
                        'nama_rekanan' => $_POST['nama_rekanan'],
                        'url_rekanan' => preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $this->limit_words($_POST['nama_rekanan'], 6))),
                        'web_rekanan' => $_POST['web_rekanan'],
                        'konten_rekanan' => $_POST['konten_rekanan'],
                        'konten_en_rekanan' => $_POST['konten_en_rekanan'],
                        'status_rekanan' => $_POST['status_rekanan'],
                        'logo_rekanan' => ($_POST['logo_rekanan']!=null?$_POST['logo_rekanan']:''),
                        'dibuat_rekanan' => date("Y-m-d"),

                    );
                    if ($this->model->updateRekanan($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/rekanan#success');
                        exit;
                    }
                    break;
                case "edit";
                    $this->view->post = json_decode($this->model->detailRekanan($lang));
                    $this->view->setting = json_decode($this->model->getSetting());
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/formrekanan.js');
                    $this->view->setBody('dashboard/formrekanan');
                    $this->view->setFooter('dashboard');
                    break;
                case "add";
                    $this->view->setting = json_decode($this->model->getSetting());
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/formrekanan.js');
                    $this->view->setBody('dashboard/formrekanan');
                    $this->view->setFooter('dashboard');
                    break;
                default:
                    $this->showError();
                    break;
            }
        } else {
            $this->view->jsPlugin = array(
                'jquery-datatable/jquery.dataTables.js',
                'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');
            $this->view->js = array(
                'dashboard/js/news.js'
            );
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->js = array('dashboard/js/rekanan.js');
            $this->view->setBody('dashboard/rekanan');
            $this->view->setFooter('dashboard');
        }
    }

    function event($parameter = false, $lang = false, $page = 0)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "save";
                    $data = array(
                        'title_post' => $_POST['title_post'],
                        'url_post' => preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $this->limit_words($_POST['title_post'], 6))),
                        'content_post' => $_POST['content_post'],
                        'jenis_post' => 2,
                        'lang_post' => $_POST['lang_post'],
                        'image_post' => ($_POST['image_post']!=null?$_POST['image_post']:URL."public/images/961s3.jpg"),
                        'created_post' => date("Y-m-d"),

                    );
                    if ($this->model->savePost($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/event#success');
                        exit;
                    }
                    break;
                case "delete";
                    $data = array(
                        'id_post' => $lang
                    );
                    if ($this->model->deletePost($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/event#success');
                        exit;
                    }
                    break;
                case "update";
                    $data = array(
                        'id_post' => $_POST['id_post'],
                        'title_post' => $_POST['title_post'],
                        'url_post' => preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $this->limit_words($_POST['title_post'], 6))),
                        'content_post' => $_POST['content_post'],
                        'jenis_post' => 2,
                        'lang_post' => $_POST['lang_post'],
                        'image_post' => ($_POST['image_post']!=null?$_POST['image_post']:URL."public/images/961s3.jpg"),
                        'created_post' => date("Y-m-d"),

                    );
                    if ($this->model->updatePost($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/event#success');
                        exit;
                    }
                    break;
                case "edit";
                    $this->view->post = json_decode($this->model->detailPost($lang));
                    $this->view->setting = json_decode($this->model->getSetting());
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/post.js');
                    $this->view->setBody('dashboard/formevent');
                    $this->view->setFooter('dashboard');
                    break;
                case "add";
                    $this->view->setting = json_decode($this->model->getSetting());
                    $this->view->setHeader('dashboard');
                    $this->view->setSidebar();
                    $this->view->jsPlugin = array('bootstrap-select/js/bootstrap-select.js', 'dropzone/dropzone.js', 'tinymce/tinymce.js');
                    $this->view->js = array('dashboard/js/post.js');
                    $this->view->setBody('dashboard/formevent');
                    $this->view->setFooter('dashboard');
                    break;
                default:
                    $this->showError();
                    break;
            }
        } else {
            $this->view->jsPlugin = array(
                'jquery-datatable/jquery.dataTables.js',
                'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');
            $this->view->js = array(
                'dashboard/js/news.js'
            );
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->js = array('dashboard/js/event.js');
            $this->view->setBody('dashboard/event');
            $this->view->setFooter('dashboard');
        }
    }

    function comments($parameter = false, $lang = false, $page = 0)
    {
        $this->geLoged();
        if ($parameter) {
            switch ($parameter) {
                case "delete";
                    $data = array(
                        'id_comments' => $lang
                    );
                    if ($this->model->deleteComments($data)) {
                        header('location:' . URL . $this->view->menus['lang'] . '/dashboard/comments#success');
                        exit;
                    }
                    break;
                default:
                    $this->showError();
                    break;
            }
        } else {
            //$this->view->listGroup = json_decode($this->model->listGroup());
            $this->view->jsPlugin = array(
                'jquery-datatable/jquery.dataTables.js',
                'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');
            $this->view->js = array(
                'dashboard/js/comments.js'
            );
            $this->view->setHeader('dashboard');
            $this->view->setSidebar();
            $this->view->setBody('dashboard/comments');
            $this->view->setFooter('dashboard');
        }
    }


    function userin()
    {
        if(isset($_POST)){
            echo $this->model->login($_POST);
        }else{
            header('location:'.URL.'id/dashboard/');
        }
    }

    function listgroup()
    {
        //print_r($_GET);
        echo $this->model->listGroup($_GET);
    }
    function listrekan()
    {
        //print_r($_GET);
        echo $this->model->listRekanan($_GET);
    }

    function listgroupkategori()
    {
        //print_r($_GET);
        echo $this->model->listGroupKategori($_GET);
    }

    function listnews()
    {
        //print_r($_GET);
        echo $this->model->listNews($_GET);
    }

    function listevent()
    {
        //print_r($_GET);
        echo $this->model->listEvent($_GET);
    }
    function listcomments()
    {
        //print_r($_GET);
        echo $this->model->listComments($_GET);
    }

    function limit_words($string, $word_limit)
    {
        $words = explode(" ", $string);
        return implode(" ", array_splice($words, 0, $word_limit));
    }

    function uploadimage()
    {
        $ds = DIRECTORY_SEPARATOR;  // Store directory separator (DIRECTORY_SEPARATOR) to a simple variable. This is just a personal preference as we hate to type long variable name.
        $storeFolder = 'public'.$ds.'images'.$ds.'uploads';   // Declare a variable for destination folder.
        if (!empty($_FILES)) {

            $tempFile = $_FILES['file']['tmp_name'];          // If file is sent to the page, store the file object to a temporary variable.
            $targetPath = realpath(dirname(__FILE__) . '/..') . $ds . $storeFolder . $ds;  // Create the absolute path of the destination folder.
            // Adding timestamp with image's name so that files with same name can be uploaded easily.
            $date = new DateTime();
            $newFileName = $date->getTimestamp() .'.jpg';
            $targetFile = $targetPath . $newFileName;  // Create the absolute path of the uploaded file destination.
            move_uploaded_file($tempFile, $targetFile); // Move uploaded file to destination.

            echo $newFileName;
        }
    }

    function deleteuploadimage()
    {
        $ds = DIRECTORY_SEPARATOR;  // Store directory separator (DIRECTORY_SEPARATOR) to a simple variable. This is just a personal preference as we hate to type long variable name.
        $storeFolder = 'public'.$ds.'images'.$ds.'uploads';

        $fileList = $_POST['fileList'];
        $targetPath = realpath(dirname(__FILE__) . '/..') . $ds . $storeFolder . $ds;


        if (isset($fileList)) {
            unlink($targetPath . $fileList);
        }

    }
    function geLoged(){
        if(!Session::get('loggedIn')){
            header('location:'.URL.'id/dashboard/login');
        }
    }
    function logout(){
        Session::destroy();
        header('location:'.URL);
        exit;
    }

}


?>