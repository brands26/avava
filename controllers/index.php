<?php

class Index extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        $this->view->setHeader();
        $this->view->js = array('index/js/index.js');
        $this->view->setBody('index/index');
        $this->view->setFooter();
    }
}




?>