<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 16/11/2016
 * Time: 9:24
 */
class Foundation extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        $this->view->page = json_decode($this->model->detailPage(1));
        $this->view->setHeader('default');
        //$this->view->js = array('foundation/js/index.js');
        $this->view->setBody('foundation/index');
        $this->view->setFooter('default');
    }
}


?>