<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 17/11/2016
 * Time: 11:12
 */
class About extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        $this->view->page = json_decode($this->model->detailPage(3));
        $this->view->setHeader('default');
        $this->view->js = array('about/js/index.js');
        $this->view->setBody('about/index');
        $this->view->setFooter('default');
    }
}




?>