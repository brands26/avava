<?php
ini_set('max_execution_time', 300);
class Api extends Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function listProvinsi($id = false)
    {
        echo $this->model->listProvinsi($id);
    }

    function listKota($id = false, $id_provinsi = false)
    {
        if ($id or $id_provinsi) {
            $data = array();
            $data['id_ref_kota_kabupaten'] = $id;
            $data['id_ref_provinsi'] = $id_provinsi;
        } else {
            $data = false;
        }
        echo $this->model->listKota($data);
    }
    function test()
    {
        function dlPage($href) {

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_URL, $href);
            curl_setopt($curl, CURLOPT_REFERER, $href);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4");
            $str = curl_exec($curl);
            curl_close($curl);

            // Create a DOM object
            $dom = new simple_html_dom();
            // Load HTML from a string
            $dom->load($str);

            return $dom;
        }

        $url = 'http://www.bi.go.id/id/publikasi/laporan-keuangan/alamat-bank/umum/Default.aspx';
        $data = dlPage($url);
        foreach($data->find('table') as $e) {
            for ($a = 3; $a < 600; $a+=3){
                if(!$e->find("td", $a)->plaintext == null && $e->find("td", $a)->innertext != '<strong>Nama Bank</strong>'){
                    $d = $e->find("td", $a)->plaintext;
                    $data = array();
                    $data['nama_bank'] = $d;
                    $this->model->insert($data);
                }
            }
        }

    }

}


?>