<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 15/11/2016
 * Time: 20:52
 */
class Home extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){

        $this->view->listKategoriGroup = json_decode($this->model->listKategoriGroup());
        $this->view->listGroup = json_decode($this->model->listGroup());
        $data= array();
        $data['lang_post'] = ($this->view->menus['lang'] == 'id'? 1:2);
        $data['jenis_post'] = 1;
        $this->view->recentNews = json_decode($this->model->recentNews($data));
        $this->view->setHeader('default');
        $this->view->js = array('home/js/index.js');
        $this->view->setBody('home/index');
        $this->view->setFooter('default');
    }
}




?>