<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 16/11/2016
 * Time: 7:55
 */
class Contact extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        $this->view->setHeader('default');
        $this->view->js = array('contact/js/index.js');
        $this->view->setBody('contact/index');
        $this->view->setFooter('default');
    }
    function send_mail(){
        $name = $_POST['email_name'];
        $mail = $_POST['email_mail'];
        $subjectForm = $_POST['email_subjectForm'];
        $messageForm = $_POST['email_messageForm'];
        $to = 'brands.pratama@gmail.com';
        $subject = $subjectForm . ' ' . $name;
        $message = '
        <html>
        <head>
          <title>Mail from '. $name .'</title>
        </head>
        <body>
            Name        : $name <br/>
            Email       : $mail <br/>
            Subject     : $subjectForm <br/>
            Message     : <br/>
                           $name <br/>
        </body>
        </html>
        ';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: ' . $mail . "\r\n";
        if(mail($to,$subject,$message,$headers)){
            header("location:".URL.$this->view->menus['lang'].'/contact#success');
        } else {
            header("location:".URL.$this->view->menus['lang'].'/contact#fail');
        }
    }
}




?>