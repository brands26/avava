<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 16/11/2016
 * Time: 8:57
 */
class News extends Controller {

    function __construct(){
        parent::__construct();

        $this->view->element = array('');
    }
    function index(){
        $data = array();
        $data['page'] = 1;
        $data['lang_post'] = ($this->view->menus['lang'] == 'id'? 1:2);
        $data['jenis_post'] = 1;
        $this->view->News  = json_decode($this->model->listNews($data));
        $this->view->recentNews  = json_decode($this->model->recentNews($data));
        $this->view->setHeader('default');
        $this->view->js = array('news/js/index.js');
        $this->view->setBody('news/index');
        $this->view->setFooter('default');
    }
    function post($url){
        $this->view->news  = json_decode($this->model->detailNews($url));
        $this->view->beforeafterNews  = json_decode($this->model->beforeAfterNews($url));
        $this->view->comments  = json_decode($this->model->comments($this->view->news[0]->id_post));
        $this->view->setHeader('default');
        $this->view->setBody('news/post');
        $this->view->setFooter('default');
    }
    function page($page){
        $data = array();
        $data['page'] = $page;
        $data['lang_post'] = ($this->view->menus['lang'] == 'id'? 1:2);
        $data['jenis_post'] = 1;
        $this->view->News  = json_decode($this->model->listNews($data));
        $this->view->recentNews  = json_decode($this->model->recentNews($data));
        $this->view->setHeader('default');
        $this->view->js = array('news/js/index.js');
        $this->view->setBody('news/index');
        $this->view->setFooter('default');
    }
    function search($keyword='',$page=1){
        $data = array();
        $data['keyword'] = $keyword;
        $data['page'] = $page;
        $data['lang_post'] = ($this->view->menus['lang'] == 'id'? 1:2);
        $data['jenis_post'] = 1;
        $this->view->News  = json_decode($this->model->searchNews($data));
        $this->view->recentNews  = json_decode($this->model->recentNews($data));
        $this->view->setHeader('default');
        $this->view->js = array('news/js/index.js');
        $this->view->setBody('news/index');
        $this->view->setFooter('default');
    }
    function comment($id){
        $data = array();
        $data['id_post'] = $id;
        $data['name_comments'] = $_POST['name_comments'];
        $data['email_comments'] = $_POST['email_comments'];
        $data['content_comments'] = $_POST['content_comments'];
        $data['created_comments'] = date("Y-m-d H:i:s");
        if(json_decode($this->model->addComments($data))){
            header('location:'.URL.$this->view->menus['lang'].'/news/post/'.$_POST['url']);
        }
    }
}


?>