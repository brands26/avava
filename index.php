<?php

//Config
require 'config/paths.php';
require 'config/database.php';
require 'config/constants.php';

//AutoLoader
function __autoload($class){
    require LIBS.$class.'.php';
}

$app = new Neko();

?>