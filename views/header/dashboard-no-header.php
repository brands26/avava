<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>ADMIN - AVAVA GROUP INTERNATIONAL</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <?php
    if(isset($this->cssDefault)){
        foreach ($this->cssDefault as $css){
            echo '<link href="'.URL.'public/plugins/'.$css.'" rel="stylesheet">';
        }
    }
    ?>
    <!-- Custom Css -->
    <link href="<?php echo URL.'public/css/'?>admin-style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo URL.'public/css/'?>themes/all-themes.css" rel="stylesheet" />
</head>

<body class="login-page" style="background: url(<?php echo URL?>public/images/867s1.jpg) no-repeat center center fixed;background-size: cover;">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>