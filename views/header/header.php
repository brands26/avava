<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>AVAVA GROUP INTERNATIONAL</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/bootstrap-theme.min.css">

    <!-- Custom css -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/style.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo URL; ?>public/css/ionicons.min.css">

    <!-- Flexslider -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/flexslider.css">

    <!-- Owl -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/owl.carousel.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo URL; ?>public/css/jquery.fullPage.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class=" pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
</div>

<!--  loader  -->
<div id="myloader" style="display: none;">
            <span class="loader">
                <img src="<?php echo URL ?>public/images/logo.png" class="normal" alt="logo">
                <img src="<?php echo URL ?>public/images/logo.png" class="retina" alt="logo">
            </span>
</div>

<!--  Main Wrap  -->
<div id="main-wrap">
    <!--  Header & Menu  -->
    <header id="header" class="fixed transparent animated">
        <nav class="navbar navbar-default white">
            <!--  Header Logo  -->
            <div id="logo">
                <a class="navbar-brand" href="<?php echo URL.$this->menus['lang'].'/home';?>">
                    <img src="<?php echo URL ?>public/images/logo_koin.png" class="logo normal" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_text.png" class="logo-text" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_koin.png" class="logo normal white-logo" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_text.png" class="logo-text white-logo" alt="logo">
                </a>
            </div>
            <!--  END Header Logo  -->
            <!--  Classic menu, responsive menu classic  -->
            <div id="menu-classic">
                <div class="menu-holder">
                    <ul>
                        <li class="submenu">
                            <a href="<?php echo URL.$this->menus['lang'].'/' ?>home"><?php echo $this->menus['home'] ?></a>
                        </li>
                        <li class="submenu">
                            <a href="<?php echo URL.$this->menus['lang'].'/' ?>avavadutaindonesia">PT. Avava Duta Indonesia</a>
                        </li>
                        <li class="submenu">
                            <a href="<?php echo URL.$this->menus['lang'].'/' ?>about"><?php echo $this->menus['about'] ?>
                            </a>
                        </li>
                        <li class="submenu">
                            <a href="#"><?php echo $this->menus['newsMenu'] ?>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="<?php echo URL.$this->menus['lang'].'/' ?>news"><?php echo $this->menus['news'] ?></a></li>
                                <li><a href="<?php echo URL.$this->menus['lang'].'/' ?>event"><?php echo $this->menus['event'] ?></a></li>
                            </ul>
                        </li>
                        <li class="submenu">
                            <a href="<?php echo URL.$this->menus['lang'].'/' ?>foundation"><?php echo $this->menus['foundation'] ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL.$this->menus['lang'].'/' ?>career"><?php echo $this->menus['career'] ?>
                            </a>
                        </li>
                        <li class="submenu">
                            <a href="<?php echo URL.$this->menus['lang'].'/' ?>contact"><?php echo $this->menus['contact'] ?></a>
                        </li>
                        <!-- Lang -->
                        <li class="lang">
                            <?php if ($this->menus['lang'] == 'id') {
                                echo '<span class="current"><a href="' . URL . 'id/home">ID</a></span>';
                                ?>
                                <ul>
                                    <li><a href="<?php echo URL ?>en/home">EN</a></li>
                                </ul>
                                <?php
                            } else {
                                echo '<span class="current"><a href="' . URL . 'en/home">EN</a></span>';
                                ?>
                                <ul>
                                    <li><a href="<?php echo URL ?>id/home">ID</a></li>
                                </ul>
                                <?php
                            }
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
            <!--  END Classic menu, responsive menu classic  -->
            <!--  Button for Responsive Menu Classic  -->
            <div id="menu-responsive-classic">
                <div class="menu-button">
                    <span class="bar bar-1"></span>
                    <span class="bar bar-2"></span>
                    <span class="bar bar-3"></span>
                </div>
            </div>
            <!--  END Button for Responsive Menu Classic  -->
        </nav>
    </header>
    <!--  END Header & Menu  -->
