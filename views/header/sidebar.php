<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 18/11/2016
 * Time: 2:55
 */ ?>

<div id="page-sidebar" style="height: 852px;">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
        <div class="scroll-sidebar" style="height: 100%; overflow: hidden; width: auto;">


            <ul id="sidebar-menu" class="sf-js-enabled sf-arrows">
                <li class="header"><span>Dashboard</span></li>
                <li>
                    <a href="<?php echo URL_DASHBOARD;?>" title="Admin Dashboard" class="sfActive">
                        <i class="glyph-icon icon-linecons-tv"></i>
                        <span>Admin dashboard</span>
                    </a>
                </li>
                <li class="header"><span>Page</span></li>
                <li>
                    <a href="index.html" title="Admin Dashboard" class="sfActive">
                        <i class="glyph-icon icon-linecons-tv"></i>
                        <span>Home</span>
                    </a>
                </li>
                <li class="no-menu">
                    <a href="../frontend-template/index.html" title="Frontend template" class="sfActive">
                        <i class="glyph-icon icon-linecons-beaker"></i>
                        <span>About</span>
                    </a>
                </li>
                <li>
                    <a href="index.html" title="Admin Dashboard" class="sfActive">
                        <i class="glyph-icon icon-linecons-tv"></i>
                        <span>News</span>
                    </a>
                </li>
                <li class="no-menu">
                    <a href="../frontend-template/index.html" title="Frontend template" class="sfActive">
                        <i class="glyph-icon icon-linecons-beaker"></i>
                        <span>Foundation</span>
                    </a>
                </li>
                <li>
                    <a href="index.html" title="Admin Dashboard" class="sfActive">
                        <i class="glyph-icon icon-linecons-tv"></i>
                        <span>Careers</span>
                    </a>
                </li>
                <li class="no-menu">
                    <a href="../frontend-template/index.html" title="Frontend template" class="sfActive">
                        <i class="glyph-icon icon-linecons-beaker"></i>
                        <span>Contact Us</span>
                    </a>
                </li>
                <li class="header"><span>Post</span></li>
                <li>
                    <a href="<?php echo URL.$this->menus['lang'].'/dashboard/ news'?>" title="Admin Dashboard" class="sfActive">
                        <i class="glyph-icon icon-linecons-tv"></i>
                        <span>News</span>
                    </a>
                </li>
                <li class="no-menu">
                    <a href="../frontend-template/index.html" title="Frontend template" class="sfActive">
                        <i class="glyph-icon icon-linecons-beaker"></i>
                        <span>Events</span>
                    </a>
                </li>
                <li class="no-menu">
                    <a href="../frontend-template/index.html" title="Frontend template" class="sfActive">
                        <i class="glyph-icon icon-linecons-beaker"></i>
                        <span>Comments</span>
                    </a>
                </li>
                <li class="header"><span>Setting</span></li>
                <li>
                    <a href="index.html" title="Admin Dashboard" class="sfActive">
                        <i class="glyph-icon icon-linecons-tv"></i>
                        <span>Web Setting</span>
                    </a>
                </li>
            </ul><!-- #sidebar-menu -->


        </div>
        <div class="slimScrollBar"
             style="background: rgba(155, 164, 169, 0.682353); width: 6px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 852px;"></div>
        <div class="slimScrollRail"
             style="width: 6px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
    </div>
</div>
