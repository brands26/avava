<html lang="en" class="sb-init">
<head>

    <style>
        /* Loading Spinner */
        .spinner {
            margin: 0;
            width: 70px;
            height: 18px;
            margin: -35px 0 0 -9px;
            position: absolute;
            top: 50%;
            left: 50%;
            text-align: center
        }

        .spinner > div {
            width: 18px;
            height: 18px;
            background-color: #333;
            border-radius: 100%;
            display: inline-block;
            -webkit-animation: bouncedelay 1.4s infinite ease-in-out;
            animation: bouncedelay 1.4s infinite ease-in-out;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both
        }

        .spinner .bounce1 {
            -webkit-animation-delay: -.32s;
            animation-delay: -.32s
        }

        .spinner .bounce2 {
            -webkit-animation-delay: -.16s;
            animation-delay: -.16s
        }

        @-webkit-keyframes bouncedelay {
            0%, 80%, 100% {
                -webkit-transform: scale(0.0)
            }
            40% {
                -webkit-transform: scale(1.0)
            }
        }

        @keyframes bouncedelay {
            0%, 80%, 100% {
                transform: scale(0.0);
                -webkit-transform: scale(0.0)
            }
            40% {
                transform: scale(1.0);
                -webkit-transform: scale(1.0)
            }
        }
    </style>


    <meta charset="UTF-8">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Avava International Group - Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo URL?>public/css/admin.css">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">


</head>


<body class="fixed-header fixed-sidebar">
<div id="sb-site" style="min-height: 1746px;">
    <div id="page-wrapper">
        <div id="page-header" class="bg-gradient-9">
            <div id="mobile-navigation">
                <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar">
                    <span></span></button>
                <a href="index.html" class="logo-content-small" title="MonarchUI"></a>
            </div>
            <div id="header-logo" class="logo-bg bg-gradient-9">
                <a href="index.html" class="logo-content-big" title="MonarchUI">
                    <img src="<?php echo URL ?>public/images/logo_koin.png" class="logo normal" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_text.png" class="logo-text" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_koin.png" class="logo normal white-logo" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_text.png" class="logo-text white-logo" alt="logo">
                </a>
                <a href="index.html" class="logo-content-small" title="MonarchUI">
                    <img src="<?php echo URL ?>public/images/logo_koin.png" class="logo normal" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_text.png" class="logo-text" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_koin.png" class="logo normal white-logo" alt="logo">
                    <img src="<?php echo URL ?>public/images/logo_text.png" class="logo-text white-logo" alt="logo">
                </a>
                <a id="close-sidebar" href="#" title="Close sidebar">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id="header-nav-left">
                <div class="user-account-btn dropdown">
                    <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                        <img width="28" src="../../assets/image-resources/gravatar.jpg" alt="Profile image">
                        <span>Thomas Barnes</span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-left">
                        <div class="box-sm">
                            <div class="login-box clearfix">
                                <div class="user-img">
                                    <a href="#" title="" class="change-img">Change photo</a>
                                    <img src="../../assets/image-resources/gravatar.jpg" alt="">
                                </div>
                                <div class="user-info">
                            <span>
                                Thomas Barnes
                                <i>UX/UI developer</i>
                            </span>
                                    <a href="#" title="Edit profile">Edit profile</a>
                                    <a href="#" title="View notifications">View notifications</a>
                                </div>
                            </div>
                            <div class="divider"></div>
                            <ul class="reset-ul mrg5B">
                                <li>
                                    <a href="#">
                                        <i class="glyph-icon float-right icon-caret-right"></i>
                                        View login page example

                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="glyph-icon float-right icon-caret-right"></i>
                                        View lockscreen example

                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="glyph-icon float-right icon-caret-right"></i>
                                        View account details

                                    </a>
                                </li>
                            </ul>
                            <div class="pad5A button-pane button-pane-alt text-center">
                                <a href="#" class="btn display-block font-normal btn-danger">
                                    <i class="glyph-icon icon-power-off"></i>
                                    Logout
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- #header-nav-left -->


        </div>
