<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/19/2016
 * Time: 10:52 PM
 */
?>

<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="<?php echo URL . 'en/dashboard' ?>">
                        <i class="material-icons">home</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle toggled ">
                        <i class="material-icons">book</i>
                        <span>Pages</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?php echo URL . 'en/dashboard/page/edit/3' ?>">
                                Tentang Avava
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL . 'en/dashboard/page/edit/1' ?>">
                                Foundation
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL . 'en/dashboard/page/edit/2' ?>">
                                Karir
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle toggled ">
                        <i class="material-icons">group</i>
                        <span>Group Companies</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?php echo URL.'id/dashboard/group'?>">Grup Perusahan</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle toggled ">
                        <i class="material-icons">group</i>
                        <span>AVAVA DUTA INDONESIA</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?php echo URL.'id/dashboard/rekanan'?>">Daftar Rekan</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle toggled ">
                        <i class="material-icons">description</i>
                        <span>Post</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?php echo URL.'id/dashboard/news'?>">Berita</a>
                        </li>
                        <li>
                            <a href="<?php echo URL.'id/dashboard/event'?>">Acara</a>
                        </li>
                        <li>
                            <a href="<?php echo URL.'id/dashboard/comments'?>">Komentar</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle toggled ">
                        <i class="material-icons">settings</i>
                        <span>Setting</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="<?php echo URL.'id/dashboard/setting'?>">Pengaturan</a>
                        </li>
                        <li>
                            <a href="<?php echo URL.'id/dashboard/account'?>">Akun</a>
                        </li>
                        <li>
                            <a href="<?php echo URL.'id/dashboard/logout'?>">Keluar</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <div class="legal">
            <div class="copyright">
                &copy; 2016 <a href="javascript:void(0);">AVAVA GROUP INTERNATIONAL</a>
            </div>
            <div class="version">
                <b>Version: </b> 1.0.0
            </div>
        </div>
    </aside>
    <!-- #END# Left Sidebar -->
</section>
