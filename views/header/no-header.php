<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>AVAVA GROUP INTERNATIONAL</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/bootstrap/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/bootstrap/bootstrap-theme.min.css">

    <!-- Custom css -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/style.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo URL;?>public/css/ionicons.min.css">

    <!-- Flexslider -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/flexslider.css">

    <!-- Owl -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/owl.carousel.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo URL;?>public/css/jquery.fullPage.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class=" pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
</div>

<!--  loader  -->
<div id="myloader" style="display: none;">
            <span class="loader">
                <img src="<?php echo URL?>public/images/logo.png" class="normal" alt="logo">
                <img src="<?php echo URL?>public/images/logo.png" class="retina" alt="logo">
            </span>
</div>

<!--  Main Wrap  -->
<div id="main-wrap" class="index">
