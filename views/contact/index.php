<!--  Page Content, class footer-fixed if footer is fixed  -->
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="kontak fullpage-wrap small">
        <ul class="slides">
            <li style="background-image: url(<?php echo URL?>public/images/banner8.jpg); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="kontak flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small white flex-animation no-opacity animated fadeInUp"><?php echo $this->menus['contact']?></h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <!-- END Services -->
        <div class="row margin-leftright-null">
            <div class="col-md-6 padding-leftright-null">
                <div class="text">
                    <h2 class="margin-bottom-null title left">More Info</h2>
                </div>
                <div class="col-md-6 padding-leftright-null">
                    <div class="text padding-md-bottom-null">
                        <i class="fa fa-globe icon service material left" aria-hidden="true"></i>
                        <div class="service-content">
                            <h6 class="heading  margin-bottom-extrasmall">Head Office</h6>
                            <p class="margin-bottom-null">Komplek Bisnis Center Blok III Lt. 2 - Jodoh<br>
                                Batam</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 padding-leftright-null">
                    <div class="text padding-md-bottom-null">
                        <i class="fa fa-phone icon service material left" aria-hidden="true"></i>
                        <div class="service-content">
                            <h6 class="heading grey margin-bottom-extrasmall">Telp</h6>
                            <p class="margin-bottom-null"><a href="">0778 - 426 666</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 padding-leftright-null">
                <div class="text padding-md-top-null">
                    <form id="contact-form" class="padding-md padding-md-topbottom-null" method="post" action="<?php echo URL.$this->menus['lang'].'/contact/send_mail'?>">
                        <div class="row">
                            <div class="col-md-4">
                                <input class="form-field" name="email_name" id="name" type="text" placeholder="Name" required>
                            </div>
                            <div class="col-md-4">
                                <input class="form-field" name="email_mail" id="mail" type="email" placeholder="Email" required>
                            </div>
                            <div class="col-md-4">
                                <input class="form-field" name="email_subjectForm" id="subjectForm" type="text"
                                       placeholder="Subject">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea class="form-field" name="email_messageForm" id="messageForm" rows="6"
                                          placeholder="Your Message" required></textarea>
                                <div class="submit-area padding-onlytop-sm">
                                    <input type="submit" id="submit-contact" class="btn-alt active shadow"
                                           value="Send Message">
                                    <div id="msg" class="message"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row margin-leftright-null">
            <!--  Map. Settings in maps.js  -->
            <div class="col-md-12 padding-leftright-null map">
                <div id="map" style="position: relative; overflow: hidden;">
                </div>
            </div>
            <!--  END Map  -->
        </div>
    </div>
</div>