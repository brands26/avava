<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 16/11/2016
 * Time: 9:24
 */

?>
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="fullpage-wrap small">
        <ul class="slides">
            <li style="background-image: url(<?php echo URL.'public/images/uploads/'.$this->page[0]->image_page; ?>); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small flex-animation no-opacity animated fadeInUp"><?php echo ($this->menus['lang']=='id'? $this->page[0]->title_page : $this->page[0]->title_page_en )?></h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="post-wrap" class="content-section fullpage-wrap">
        <div class="row padding-md content-post no-margin">
            <div class="col-md-offset-3 col-md-6 padding-leftright-null text-center">

            </div>
            <div class="col-md-offset-3 col-md-6 padding-leftright-null">
                <?php echo ($this->menus['lang']=='id'? $this->page[0]->content_page : $this->page[0]->content_page_en )?>
            </div>
        </div>
        <!--  END Post Meta  -->
    </div>
</div>
