<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 18/11/2016
 * Time: 5:27
 */ ?>
<section class="content">
    <div class="container-fluid">
        <!-- Start Content -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php echo (isset($this->post[0]->id_post) ? 'Ubah Acara': 'Tambah Acara') ?>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="<?php echo (isset($this->post[0]->id_post) ? URL .'id/dashboard/event/update' : URL .'id/dashboard/event/save') ?>" method="post">
                            <div class="row clearfix">
                                <div class="col-sm-8">

                                    <input type="text" id="" name="id_post"
                                           value="<?php echo(isset($this->post[0]->id_post) ? $this->post[0]->id_post : '') ?>"
                                           hidden>
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="title_post"
                                                   value="<?php echo(isset($this->post[0]->id_post) ? $this->post[0]->title_post : '') ?>"
                                                   required>
                                            <label class="form-label">judul</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <select name="lang_post" class="form-control show-tick">
                                        <option value="">-- Pilih Bahasa Berita--</option>
                                        <option
                                            value="1" <?php echo(isset($this->post[0]->lang_post) ? ($this->post[0]->lang_post == 1) ? 'selected' : '' : '') ?>>
                                            Bahasa Indonesia
                                        </option>
                                        <option
                                            value="2" <?php echo(isset($this->post[0]->lang_post) ? ($this->post[0]->lang_post == 2) ? 'selected' : '' : '') ?>>
                                            Bahasa Inggris
                                        </option>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Foto</h2>
                                    <div id="file" class="dropzone ">
                                        <div class="dz-message">
                                            <div class="drag-icon-cph">
                                                <i class="material-icons">touch_app</i>
                                            </div>
                                            <h3>Drop files here or click to upload.</h3>
                                        </div>
                                    </div>
                                    <input type="text" id="file-input" name="image_post" hidden>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Content</h2>
                                    <textarea id="content"
                                              name="content_post"><?php echo(isset($this->post[0]->content_post) ? $this->post[0]->content_post : '') ?></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</section>

<script>
    <?php
        if(isset($this->post[0]->image_post)){
            $images = explode(',',$this->post[0]->image_post);
            $result = array();
            if($this->post[0]->image_post !='http://localhost/avava/public/images/961s3.jpg'){
                foreach ($images as $image){
                    $data = array();
                    $data['name'] = $image;
                    $data['size'] = 0;
                    $result[] = $data;
                }
            }

        }else{
            $result = array();
        }
    ?>
    var mockFile = <?php echo json_encode($result)?>;
</script>