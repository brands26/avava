<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 18/11/2016
 * Time: 3:57
 */
?>

<div id="page-content-wrapper">
    <div id="page-content" style="min-height: 1785px;">

        <div class="container">

            <div id="page-title">
                <h2>Contact Us</h2>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <h3 class="title-hero">
                                Elements
                            </h3>
                            <div class="example-box-wrapper">
                                <form class="form-horizontal bordered-row" action="<?php echo URL.$this->menus['lang'].'/dashboard/contact/save'?>" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Head office</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="" placeholder="Head office" name="head_office" value="<?php echo  $this->setting->head_office;?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Telp</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="" placeholder="telp" name="telp" value="<?php echo $this->setting->telp;?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="" placeholder="Email" name="email" value="<?php echo $this->setting->email;?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3 pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</div>
