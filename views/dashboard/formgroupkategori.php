<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 21/11/2016
 * Time: 12:37
 */
?>
<section class="content">
    <div class="container-fluid">
        <!-- Start Content -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php echo (isset($this->post[0]->id_group_kategori) ? 'Ubah Kategori Group': 'Tambah Kategori Group') ?>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="<?php echo (isset($this->post[0]->id_group_kategori) ? URL .'id/dashboard/groupcategory/update' : URL .'id/dashboard/groupcategory/save') ?>" method="post">
                            <div class="row clearfix">
                                <div class="col-sm-12">

                                    <input type="text" id="" name="id_group_kategori"
                                           value="<?php echo(isset($this->post[0]->id_group_kategori) ? $this->post[0]->id_group_kategori : '') ?>"
                                           hidden>
                                    <div class="form-group form-float ">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="name_group_kategori"
                                                   value="<?php echo(isset($this->post[0]->name_group_kategori) ? $this->post[0]->name_group_kategori : '') ?>"
                                                   required>
                                            <label class="form-label">Nama Kategori Group</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</section>



