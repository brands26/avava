<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 21/11/2016
 * Time: 12:02
 */
?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Berita
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table
                            class="table table-bordered table-striped table-hover js-basic-example dt-responsive  dataTable">
                            <thead>
                            <tr>
                                <th>Dibuat</th>
                                <th>Thumbnail</th>
                                <th>Judul</th>
                                <th>Konten</th>
                                <th>Tindakan</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Dibuat</th>
                                <th>Thumbnail</th>
                                <th>Judul</th>
                                <th>Konten</th>
                                <th>Tindakan</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="<?php echo URL . 'id/dashboard/event/add' ?>" class="button add btn btn-success btn-circle-lg waves-effect waves-circle waves-float">
        <i class="material-icons">add</i>
    </a>
</section>


