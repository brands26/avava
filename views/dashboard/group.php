<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/20/2016
 * Time: 8:39 AM
 */
?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Group
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Nama Group</th>
                                <th>Kategori</th>
                                <th>Konten</th>
                                <th>tindakan</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Nama Group</th>
                                <th>Kategori</th>
                                <th>Konten</th>
                                <th>tindakan</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="<?php echo URL . 'id/dashboard/group/add' ?>" class="button add btn btn-success btn-circle-lg waves-effect waves-circle waves-float">
        <i class="material-icons">add</i>
    </a>
</section>

