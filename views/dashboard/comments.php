<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 22/11/2016
 * Time: 21:45
 */
?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Daftar Komentar
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table
                            class="table table-bordered table-striped table-hover js-basic-example dt-responsive  dataTable">
                            <thead>
                            <tr>
                                <th>Judul Post</th>
                                <th>Pengirim</th>
                                <th>Email</th>
                                <th>Isi Komentar</th>
                                <th>Dibuat</th>
                                <th>Tindakan</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Judul Post</th>
                                <th>Pengirim</th>
                                <th>Email</th>
                                <th>Isi Komentar</th>
                                <th>Dibuat</th>
                                <th>Tindakan</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
