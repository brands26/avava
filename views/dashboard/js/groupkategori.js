/**
 * Created by Brands on 11/20/2016.
 */
$(function () {
    $('.js-basic-example').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": URL+"en/dashboard/listgroupkategori",
            columns: [
                { data: "name_group_kategori" },
                {
                    data: "id_group_kategori",
                    sortable: false,
                    className: "center",
                    "render": function ( data, type, full, meta ) {
                        return '<a href="'+URL+'id/dashboard/groupcategory/edit/'+data+'" >ubah</a><br/><a href="'+URL+'id/dashboard/groupcategory/delete/'+data+'" >hapus</a>';
                    }
                }
            ]
        }
    );

    //Exportable table
});