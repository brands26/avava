/**
 * Created by Brands-PC on 22/11/2016.
 */
$(function () {
    $('.js-basic-example').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": URL + "en/dashboard/listcomments",
            columns: [
                {data: "title_post"},
                {data: "name_comments"},
                {data: "email_comments"},
                {data: "content_comments"},
                {data: "created_comments"},
                {
                    data: "id_comments",
                    sortable: false,
                    className: "center",
                    "render": function (data, type, full, meta) {
                        return '<a href="' + URL + 'id/dashboard/comments/delete/' + data + '" class="delete-button">hapus</a>';
                    }
                }
            ]
        }
    );

    //Exportable table
});