/**
 * Created by Brands on 11/20/2016.
 */
$(function () {
    $('.js-basic-example').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": URL + "en/dashboard/listnews",
            columns: [
                {data: "created_post"},
                {
                    data: "image_post",
                    "render": function (data, type, full, meta) {
                        return '<img class="media-object" src="' + data + '" width="64" height="64">';
                    }
                },
                {data: "title_post"},
                {
                    data: "content_post",
                    "render": function (data, type, full, meta) {
                        if (data.length > 200)
                            return data.substr(0, 200);
                        else
                            return data;
                    }
                },
                {
                    data: "id_post",
                    sortable: false,
                    className: "center",
                    "render": function (data, type, full, meta) {
                        return '<a href="' + URL + 'id/dashboard/news/edit/' + data + '" >ubah</a><br/><a href="' + URL + 'id/dashboard/news/delete/' + data + '" class="delete-button">hapus</a>';
                    }
                }
            ]
        }
    );

    //Exportable table
});