/**
 * Created by Brands-PC on 22/11/2016.
 */
$(document).ready(function () {
    Dropzone.autoDiscover = false;
    var fileList = new Array;
    var o = 0;
    $("#file").dropzone({
        maxFiles: 1,
        addRemoveLinks: true,
        init: function () {

            // Hack: Add the dropzone class to the element
            $(this.element).addClass("dropzone");
            this.on("success", function (file, serverFileName) {
                fileList[o] = {"serverFileName": serverFileName, "fileName": file.name, "fileId": o};
                //console.log(fileList);
                i++;
                var name = '';
                for (f = 0; f < fileList.length; f++) {
                    name += fileList[f].serverFileName + ',';
                }
                name = name.substring(0, name.length - 1);
                $('#file-input').val(name);
                console.log(fileList);
            });
            this.on("removedfile", function (file) {
                var rmvFile = "";
                for (f = 0; f < fileList.length; f++) {

                    if (fileList[f].fileName == file.name) {
                        rmvFile = fileList[f].serverFileName;
                        $.ajax({
                            url: URL + "id/dashboard/deleteuploadimage",
                            type: "POST",
                            data: {"fileList": rmvFile}
                        });
                        fileList.splice(f, 1);
                    }
                }
                var name = '';
                for (f = 0; f < fileList.length; f++) {
                    name += fileList[f].serverFileName + ',';
                }
                name = name.substring(0, name.length - 1);
                $('#file-input').val(name);
                if(fileList.length<1){
                    this.options.maxFiles += 1;
                }
            });
            if(mockFile.length!=0){
                for(var i=0;i<mockFile.length;i++){
                    this.emit("addedfile", mockFile[i]);
                    this.emit("thumbnail", mockFile[i], URL + 'public/images/uploads/' +mockFile[i].name);
                    this.emit("complete", mockFile[i]);
                    fileList[i] = {"serverFileName": mockFile[i].name, "fileName": mockFile[i].name, "fileId": i};
                }
                var existingFileCount = fileList.length; // The number of files already uploaded
                this.options.maxFiles = this.options.maxFiles - existingFileCount;
                var name = '';
                for (f = 0; f < fileList.length; f++) {
                    name +=fileList[f].serverFileName + ',';
                }
                name = name.substring(0, name.length - 1);
                $('#file-input').val(name);
                console.log(fileList);
            }
        },
        url: URL + "id/dashboard/uploadimage"
    });

    tinymce.init({
        selector: "textarea#content",
        theme: "modern",
        height: 300,
        relative_urls: false,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager '
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ',
        toolbar2: 'print preview media | forecolor backcolor emoticons | responsivefilemanager ',
        image_advtab: true,
        external_filemanager_path:URL+"filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : URL+"filemanager/plugin.min.js"}
    });
    tinymce.init({
        selector: "textarea#content_en",
        theme: "modern",
        height: 300,
        relative_urls: false,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager '
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ',
        toolbar2: 'print preview media | forecolor backcolor emoticons | responsivefilemanager ',
        image_advtab: true,
        external_filemanager_path:URL+"filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : URL+"filemanager/plugin.min.js"}
    });
})
