/**
 * Created by Brands on 11/27/2016.
 */
$(function () {
    $('.js-basic-example').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": URL + "id/dashboard/listrekan",
            columns: [
                {data: "dibuat_rekanan"},
                {
                    data: "logo_rekanan",
                    "render": function (data, type, full, meta) {
                        return '<img class="media-object" src="' + data + '" width="64" height="64">';
                    }
                },
                {data: "nama_rekanan"},
                {
                    data: "web_rekanan",
                    "render": function (data, type, full, meta) {
                        return '<a href="' + data + '">'+data+'<a>';
                    }
                },{
                    data: "status_rekanan",
                    "render": function (data, type, full, meta) {
                        if (data == '1')
                            return 'Masih Dalam Kontrak';
                        else
                            return 'Selesai Kontrak';
                    }
                },
                {
                    data: "konten_rekanan",
                    "render": function (data, type, full, meta) {
                        if (data.length > 200)
                            return data.substr(0, 200);
                        else
                            return data;
                    }
                },
                {
                    data: "id_rekanan",
                    sortable: false,
                    className: "center",
                    "render": function (data, type, full, meta) {
                        return '<a href="' + URL + 'id/dashboard/rekanan/edit/' + data + '" >ubah</a><br/><a href="' + URL + 'id/dashboard/rekanan/delete/' + data + '" class="delete-button">hapus</a>';
                    }
                }
            ]
        }
    );

    //Exportable table
});
