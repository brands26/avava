$(function () {
    console.log(URL + "en/dashboard/listgroup");
    $('.js-basic-example').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": URL + "en/dashboard/listgroup",
            columns: [
                {data: "name_group"},
                {data: "name_group_kategori"},
                {
                    data: "content_group",
                    "render": function (data, type, full, meta) {
                        if (data.length > 200)
                            return data.substr(0, 200);
                        else
                            return data;
                    }
                },
                {
                    data: "id_group",
                    sortable: false,
                    className: "center",
                    "render": function (data, type, full, meta) {
                        return '<a href="' + URL + 'id/dashboard/group/edit/' + data + '" >ubah</a><br/><a href="' + URL + 'id/dashboard/group/delete/' + data + '" >hapus</a>';
                    }
                }
            ]
        }
    );

    //Exportable table
});