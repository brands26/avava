<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 22/11/2016
 * Time: 5:01
 */ ?>
<section class="content">
    <div class="container-fluid">
        <!-- Start Content -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php echo(isset($this->page[0]->id_page) ? 'Ubah Halaman' : '') ?>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form
                            action="<?php echo(isset($this->page[0]->id_post) ? URL . 'en/dashboard/page/save' : URL . 'en/dashboard/page/save') ?>"
                            method="post">
                            <div class="row clearfix">
                                <div class="col-sm-6">

                                    <input type="text" id="" name="id_page"
                                           value="<?php echo(isset($this->page[0]->id_page) ? $this->page[0]->id_page : '') ?>"
                                           hidden>
                                    <div class="form-group form-float ">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="title_page"
                                                   value="<?php echo(isset($this->page[0]->title_page) ? $this->page[0]->title_page : '') ?>"
                                                   required>
                                            <label class="form-label">Judul Bahasa Indonesia</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="title_page_en"
                                                   value="<?php echo(isset($this->page[0]->title_page_en) ? $this->page[0]->title_page_en : '') ?>"
                                            >
                                            <label class="form-label">Judul Bahasa Inggris</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Konten Bahasa Indonesia</h2>
                                    <textarea id="content"
                                              name="content_page"><?php echo(isset($this->page[0]->content_page) ? $this->page[0]->content_page : '') ?></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Konten Bahasa Inggris</h2>
                                    <textarea id="content_en"
                                              name="content_page_en"><?php echo(isset($this->page[0]->content_page_en) ? $this->page[0]->content_page_en : '') ?></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Foto Banner</h2>
                                    <div id="file" class="dropzone ">
                                        <div class="dz-message">
                                            <div class="drag-icon-cph">
                                                <i class="material-icons">touch_app</i>
                                            </div>
                                            <h3>Drop files here or click to upload.</h3>
                                        </div>
                                    </div>
                                    <input type="text" id="file-input" name="image_page" hidden>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</section>

<script>
    <?php
    if (isset($this->page[0]->image_page)) {
        $images = explode(',', $this->page[0]->image_page);
        $result = array();
        if ($this->page[0]->image_page != 'http://localhost/avava/public/images/961s3.jpg') {
            foreach ($images as $image) {
                $data = array();
                $link_array = explode('/',$image);
                $page = end($link_array);
                $data['name'] = $page;
                $data['size'] = 0;
                $result[] = $data;
            }
        }

    } else {
        $result = array();
    }
    ?>
    var mockFile = <?php echo json_encode($result)?>;
</script>
