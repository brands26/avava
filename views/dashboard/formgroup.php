<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/19/2016
 * Time: 12:40 AM
 */ ?>
<section class="content">
    <div class="container-fluid">
        <!-- Start Content -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php echo(isset($this->group[0]->id_group) ? 'Ubah Grup' : 'Tambah Grup') ?>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form
                            action="<?php echo(isset($this->group[0]->id_group) ? URL . 'id/dashboard/group/update' : URL . 'id/dashboard/group/save') ?>"
                            method="post">
                            <div class="row clearfix">
                                <div class="col-sm-6">

                                    <input type="text" id="" name="id_group"
                                           value="<?php echo(isset($this->group[0]->id_group) ? $this->group[0]->id_group : '') ?>"
                                           hidden>
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="name_group"
                                                   value="<?php echo(isset($this->group[0]->id_group) ? $this->group[0]->name_group : '') ?>"
                                                   >
                                            <label class="form-label">Nama Grup</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="name_group_en"
                                                   value="<?php echo(isset($this->group[0]->id_group) ? $this->group[0]->name_group_en : '') ?>"
                                            >
                                            <label class="form-label">Nama Grup Bahasa Inggris</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <h2 class="card-inside-title">Tampil Group</h2>
                                    <select name="lang_group" class="form-control show-tick">
                                        <option value="">-- Pilih Tampilkan Group--</option>
                                        <option
                                            value="1" <?php echo(isset($this->group[0]->lang_group) ? ($this->group[0]->lang_group == 1) ? 'selected' : '' : '') ?>>
                                            Bahasa Indonesia Saja
                                        </option>
                                        <option
                                            value="2" <?php echo(isset($this->group[0]->lang_group) ? ($this->group[0]->lang_group == 2) ? 'selected' : '' : '') ?>>
                                            Bahasa Inggris Saja
                                        </option>
                                        <option
                                            value="3" <?php echo(isset($this->group[0]->lang_group) ? ($this->group[0]->lang_group == 3) ? 'selected' : '' : '') ?>>
                                            Semua Bahasa
                                        </option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <h2 class="card-inside-title">Kategori Grup</h2>
                                    <select name="id_group_kategori" class="form-control show-tick">
                                        <option value="">-- Pilih Kategori Grup--</option>
                                        <?php foreach ($this->kategoriGroup as $kategori) {
                                            ?>
                                            <option
                                                value="<?php echo($kategori->id_group_kategori) ?>" <?php echo(isset($this->group[0]->id_group_kategori) ? $kategori->id_group_kategori == $this->group[0]->id_group_kategori ? 'selected' : '' : '') ?>>
                                                <?php echo($kategori->name_group_kategori) ?>
                                            </option>
                                            <?php
                                        } ?>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Foto</h2>
                                    <div id="file" class="dropzone ">
                                        <div class="dz-message">
                                            <div class="drag-icon-cph">
                                                <i class="material-icons">touch_app</i>
                                            </div>
                                            <h3>Drop files here or click to upload.</h3>
                                        </div>
                                    </div>
                                    <input type="text" id="file-input" name="caraousel_image_group" hidden>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Content Bahasa Indonesia</h2>
                                    <textarea id="content"
                                              name="content_group"><?php echo(isset($this->group[0]->content_group) ? $this->group[0]->content_group : '') ?></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Content Bahasa Inggris</h2>
                                    <textarea id="content_en"
                                              name="content_group_en"><?php echo(isset($this->group[0]->content_group_en) ? $this->group[0]->content_group_en : '') ?></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</section>

<script>
    <?php
    if (isset($this->group[0]->caraousel_image_group)) {
        $images = explode(',', $this->group[0]->caraousel_image_group);
        $result = array();
        if ($this->group[0]->caraousel_image_group != 'http://localhost/avava/public/images/961s3.jpg') {
            foreach ($images as $image) {
                $data = array();
                $data['name'] = $image;
                $data['size'] = 0;
                $result[] = $data;
            }
        }

    } else {
        $result = array();
    }
    ?>
    var mockFile = <?php echo json_encode($result)?>;
</script>

