<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/27/2016
 * Time: 6:55 AM
 */
?>
<section class="content">
    <div class="container-fluid">
        <!-- Start Content -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php echo (isset($this->post[0]->id_rekanan) ? 'Ubah Rekanan': 'Tambah Rekanan') ?>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="<?php echo (isset($this->post[0]->id_rekanan) ? URL .'id/dashboard/rekanan/update' : URL .'id/dashboard/rekanan/save') ?>" method="post">
                            <div class="row clearfix">
                                <div class="col-sm-4">

                                    <input type="text" id="" name="id_rekanan"
                                           value="<?php echo(isset($this->post[0]->id_rekanan) ? $this->post[0]->id_rekanan : '') ?>"
                                           hidden>
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="nama_rekanan"
                                                   value="<?php echo(isset($this->post[0]->nama_rekanan) ? $this->post[0]->nama_rekanan : '') ?>"
                                                   required>
                                            <label class="form-label">Nama Rekanan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="web_rekanan"
                                                   value="<?php echo(isset($this->post[0]->web_rekanan) ? $this->post[0]->web_rekanan : '') ?>"
                                                   >
                                            <label class="form-label">Website Rekanan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <select name="status_rekanan" class="form-control show-tick">
                                        <option value="">-- Pilih Status Kontrak--</option>
                                        <option
                                            value="1" <?php echo(isset($this->post[0]->status_rekanan) ? ($this->post[0]->status_rekanan == 1) ? 'selected' : '' : '') ?>>
                                            Masih Dalam Kontrak
                                        </option>
                                        <option
                                            value="2" <?php echo(isset($this->post[0]->status_rekanan) ? ($this->post[0]->status_rekanan == 2) ? 'selected' : '' : '') ?>>
                                            Selesai Kontrak
                                        </option>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Foto Logo / Banner</h2>
                                    <div id="file" class="dropzone ">
                                        <div class="dz-message">
                                            <div class="drag-icon-cph">
                                                <i class="material-icons">touch_app</i>
                                            </div>
                                            <h3>Drop files here or click to upload.</h3>
                                        </div>
                                    </div>
                                    <input type="text" id="file-input" name="logo_rekanan" hidden>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Konten Indonesia</h2>
                                    <textarea id="content"
                                              name="konten_rekanan"><?php echo(isset($this->post[0]->konten_rekanan) ? $this->post[0]->konten_rekanan : '') ?></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Konten Inggris</h2>
                                    <textarea id="content_en"
                                              name="konten_en_rekanan"><?php echo(isset($this->post[0]->konten_en_rekanan) ? $this->post[0]->konten_en_rekanan : '') ?></textarea>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</section>

<script>
    <?php
    if(isset($this->post[0]->logo_rekanan)){
        $images = explode(',',$this->post[0]->logo_rekanan);
        $result = array();
        if($this->post[0]->logo_rekanan !=''){
            foreach ($images as $image){
                $data = array();
                $data['name'] = $image;
                $data['size'] = 0;
                $result[] = $data;
            }
        }

    }else{
        $result = array();
    }
    ?>
    var mockFile = <?php echo json_encode($result)?>;
</script>
