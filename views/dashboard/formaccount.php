<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 22/11/2016
 * Time: 23:57
 */
?>

<section class="content">
    <div class="container-fluid">
        <!-- Start Content -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Akun
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="<?php echo  URL .'id/dashboard/account/update'?>" method="post">
                            <div class="row clearfix">
                                <input type="text" id="" name="id_user"
                                       value="<?php echo (isset($this->setting[0]->id_user) ? $this->setting[0]->id_user : '') ?>"
                                       hidden>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="username"
                                                   value="<?php echo (isset($this->setting[0]->username) ? $this->setting[0]->username : '') ?>"
                                                   required>
                                            <label class="form-label">Username</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="password" class="form-control" id="" name="password"
                                                   value=""
                                                   required>
                                            <label class="form-label">Password</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</section>
