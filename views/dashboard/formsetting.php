<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 22/11/2016
 * Time: 23:57
 */
?>

<section class="content">
    <div class="container-fluid">
        <!-- Start Content -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Pengaturan Web
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="<?php echo  URL .'id/dashboard/setting/update'?>" method="post">
                            <div class="row clearfix">
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="head_office"
                                                   value="<?php echo (isset($this->setting->head_office) ? $this->setting->head_office : '') ?>"
                                                   required>
                                            <label class="form-label">Head Office</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="telp"
                                                   value="<?php echo (isset($this->setting->telp) ? $this->setting->telp : '') ?>"
                                                   required>
                                            <label class="form-label">Telp</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="email"
                                                   value="<?php echo (isset($this->setting->email) ? $this->setting->email : '') ?>"
                                                   required>
                                            <label class="form-label">Email</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="facebook"
                                                   value="<?php echo (isset($this->setting->facebook) ? $this->setting->facebook : '') ?>"
                                                   required>
                                            <label class="form-label">Facebook</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="twitter"
                                                   value="<?php echo (isset($this->setting->twitter) ? $this->setting->twitter : '') ?>"
                                                   required>
                                            <label class="form-label">Twitter</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="" name="Linkedin"
                                                   value="<?php echo (isset($this->setting->Linkedin) ? $this->setting->Linkedin : '') ?>"
                                                   required>
                                            <label class="form-label">Linkedin</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <div class="form-line  focused">
                                            <input type="text" class="form-control" id="" name="Instagram"
                                                   value="<?php echo (isset($this->setting->email) ? $this->setting->Instagram : '') ?>"
                                                   required>
                                            <label class="form-label">Instagram</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</section>
