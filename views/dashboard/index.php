<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 18/11/2016
 * Time: 1:33
 */
?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Dashboard
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <h2 class="card-inside-title">Pages</h2>
                        <ul class="list-group">
                            <li class="list-group-item"><a href="<?php echo URL . 'en/dashboard/page/edit/3' ?>">
                                    Tentang Avava
                                </a></li>
                            <li class="list-group-item"><a href="<?php echo URL . 'en/dashboard/page/edit/1' ?>">
                                    Foundation
                                </a></li>
                            <li class="list-group-item"><a href="<?php echo URL . 'en/dashboard/page/edit/2' ?>">
                                    Karir
                                </a></li>
                        </ul>
                        <h2 class="card-inside-title">Group Companies</h2>
                        <ul class="list-group">
                            <li class="list-group-item"><a href="<?php echo URL.'id/dashboard/group'?>">Grup Perusahan</a></li>
                        </ul>
                        <h2 class="card-inside-title">AVAVA DUTA INDONESIA</h2>
                        <ul class="list-group">
                            <li class="list-group-item"><a href="<?php echo URL.'id/dashboard/rekanan'?>">daftar rekan</a></li>
                        </ul>
                        <h2 class="card-inside-title">Post</h2>
                        <ul class="list-group">
                            <li class="list-group-item"><a href="<?php echo URL.'id/dashboard/news'?>">Berita</a></li>
                            <li class="list-group-item"><a href="<?php echo URL.'id/dashboard/event'?>">Acara</a></li>
                            <li class="list-group-item"><a href="<?php echo URL.'id/dashboard/comments'?>">Komentar</a></li>
                        </ul>
                        <h2 class="card-inside-title">Setting</h2>
                        <ul class="list-group">
                            <li class="list-group-item"><a href="<?php echo URL.'id/dashboard/setting'?>">Pengaturan</a></li>
                            <li class="list-group-item"><a href="<?php echo URL.'id/dashboard/account'?>">Akun</a></li>
                            <li class="list-group-item"><a href="<?php echo URL.'id/dashboard/logout'?>">Keluar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>