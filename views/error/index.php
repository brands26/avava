
<div id="page-content" class="header-static">
    <!--  Slider  -->
    <div id="flexslider" class="fullpage-wrap small">
        <ul class="slides">
            <li style="background-image: url(&quot;assets/img/404.jpg&quot;); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small">404</h1>
                    <p class="heading white margin-bottom">Halaman Tidak Ditemukan</p>
                    <a href="<?php echo URL.$this->menus['lang'].'/home'?>" class="btn-alt small active margin-null shadow">Return Home</a>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
</div>
