/**
 * Created by Brands-PC on 16/11/2016.
 */
$( document ).ready(function() {
    if ($(window).width() < 800) {
        $("#mobile-video")[0].pause();
        window.setTimeout(function(){

            // Move to a new location or you can do something else
            window.location.href = URL+"id/home";

        }, 3000);
    }else{
        $("#mobile-video").on("ended", function() {
            window.location.href = URL+"id/home";
        });
    }
});