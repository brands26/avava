 <!--  Page Content, class footer-fixed if footer is fixed  -->
    <div id="page-content" class="header-static">
        <!--  Slider  -->
        <div id="slider-video" class="fullpage-wrap">
            <div class="local-video">
                <video id="mobile-video" autoplay="" poster="<?php echo URL?>public/images/uploads/bg_mobile.jpg" preload="auto" >
                    <source src="<?php echo URL?>public/video/intro.mp4" type="video/mp4">
                </video>
                <div id="index" class="logo">
                    <a class="navbar-brand" href="<?php echo URL?>id/home">
                        <img src="<?php echo URL?>public/images/logo_koin.png" class="logo normal" alt="logo">
                        <img src="<?php echo URL?>public/images/logo_text.png" class="logo-text" alt="logo">
                        <img src="<?php echo URL?>public/images/logo_koin.png" class="logo normal white-logo" alt="logo">
                        <img src="<?php echo URL?>public/images/logo_text.png" class="logo-text white-logo" alt="logo">
                    </a>
                </div>
                <div class="text">
                    <a href="<?php echo URL?>id/home" class="btn-alt small active">Home</a>
                </div>
                <div class="gradient"></div>
            </div>
        </div>
        <!--  END Slider  -->
    </div>
    <!--  END Page Content, class footer-fixed if footer is fixed  -->
    <!--  Page Content, class footer-fixed if footer is fixed  -->
