<!--  END Page Content, class footer-fixed if footer is fixed  -->

<footer class="fixed">
    <div class="row">
        <div class="col-md-5 col-md-push-2">
            <div class="logo">
                <img src="<?php echo URL?>public/images/logo.png" class="normal" alt="logo">
                <img src="<?php echo URL?>public/images/logo.png" class="retina" alt="logo">
            </div>
            <p class="grey-light">Avava International Group is A Company That Provides Solutions Appropriate, Effective, Efficient And Innovative, Also Have A Sensitivity To Social Activities In The Community.</p>
        </div>
        <div class="col-md-3 col-md-push-1">
            <h6 class="heading white margin-bottom-extrasmall">Halaman</h6>
            <ul class="sitemap">
                <li><a href="<?php echo URL.$this->menus['lang'].'/home/'?>">Home</a></li>
                <li><a href="<?php echo URL.$this->menus['lang'].'/about/'?>">Tentang AVAVA</a></li>
                <li><a href="<?php echo URL.$this->menus['lang'].'/news/'?>">Berita</a></li>
                <li><a href="<?php echo URL.$this->menus['lang'].'/foundation/'?>">Foundation</a></li>
                <li><a href="<?php echo URL.$this->menus['lang'].'/career/'?>">Karir</a></li>
                <li><a href="<?php echo URL.$this->menus['lang'].'/contact/'?>">Kontak Kami</a></li>
            </ul>
        </div>
        <div class="col-md-3 col-md-push-1">
            <h6 class="heading white margin-bottom-extrasmall">Social</h6>
            <ul class="info">
                <li><a href="<?php echo $this->setting->facebook?>">Facebook</a></li>
                <li><a href="<?php echo $this->setting->Linkedin?>">Linkedin</a></li>
                <li><a href="<?php echo $this->setting->Instagram?>">Instagram</a></li>
                <li><a href="<?php echo $this->setting->twitter?>">Twitter</a></li>
            </ul>
        </div>
    </div>
    <div class="copy col-md-12 padding-leftright-null">
        © 2016 Avava Group Indonesia
        <a href="#main-wrap" class="anchor" id="backtotop">Back to top</a>
    </div>
</footer>
</div>
<!--  Main Wrap  -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo URL;?>public/js/constants.js"></script>
<script src="<?php echo URL;?>public/js/jquery.min.js"></script>
<!-- All js library -->
<script src="<?php echo URL;?>public/js/bootstrap.min.js"></script>
<script src="<?php echo URL;?>public/js/jquery.flexslider-min.js"></script>
<script src="<?php echo URL;?>public/js/jquery.fullPage.min.js"></script>
<script src="<?php echo URL;?>public/js/owl.carousel.min.js"></script>
<script src="<?php echo URL;?>public/js/isotope.min.js"></script>
<script src="<?php echo URL;?>public/js/jquery.magnific-popup.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFFtpAwGdZxQcuC5_CBxXIVy_bk5eHLPA"></script>
<script src="<?php echo URL;?>public/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo URL;?>public/js/smooth.scroll.min.js"></script>
<script src="<?php echo URL;?>public/js/jquery.appear.js"></script>
<script src="<?php echo URL;?>public/js/jquery.countTo.js"></script>
<script src="<?php echo URL;?>public/js/jquery.scrolly.js"></script>
<script src="<?php echo URL;?>public/js/plugins-scroll.js"></script>
<script src="<?php echo URL;?>public/js/imagesloaded.min.js"></script>
<script src="<?php echo URL;?>public/js/pace.min.js"></script>
<script src="<?php echo URL;?>public/js/main.js"></script>
<?php
if(isset($this->js)){
    foreach ($this->js as $js){
        echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
    }
}
?>

</body></html>