</div>
<!--  Main Wrap  -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo URL;?>public/js/constants.js"></script>
<script src="<?php echo URL;?>public/js/jquery.min.js"></script>
<!-- All js library -->
<script src="<?php echo URL;?>public/js/bootstrap.min.js"></script>
<script src="<?php echo URL;?>public/js/jquery.flexslider-min.js"></script>
<script src="<?php echo URL;?>public/js/jquery.fullPage.min.js"></script>
<script src="<?php echo URL;?>public/js/owl.carousel.min.js"></script>
<script src="<?php echo URL;?>public/js/isotope.min.js"></script>
<script src="<?php echo URL;?>public/js/jquery.magnific-popup.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBFFtpAwGdZxQcuC5_CBxXIVy_bk5eHLPA"></script>
<script src="<?php echo URL;?>public/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo URL;?>public/js/smooth.scroll.min.js"></script>
<script src="<?php echo URL;?>public/js/jquery.appear.js"></script>
<script src="<?php echo URL;?>public/js/jquery.countTo.js"></script>
<script src="<?php echo URL;?>public/js/jquery.scrolly.js"></script>
<script src="<?php echo URL;?>public/js/plugins-scroll.js"></script>
<script src="<?php echo URL;?>public/js/imagesloaded.min.js"></script>
<script src="<?php echo URL;?>public/js/pace.min.js"></script>
<script src="<?php echo URL;?>public/js/main.js"></script>
<?php
if(isset($this->js)){
    foreach ($this->js as $js){
        echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
    }
}
?>

</body></html>