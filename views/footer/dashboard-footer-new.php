<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/19/2016
 * Time: 10:53 PM
 */
?>

<script src="<?php echo URL;?>public/js/constants.js"></script>
<?php

if(isset($this->jsDefault)){
    foreach ($this->jsDefault as $js){
        echo '<script type="text/javascript" src="'.URL.'public/plugins/'.$js.'"></script>';
    }
}
if(isset($this->jsPlugin)){
    foreach ($this->jsPlugin as $js){
        echo '<script type="text/javascript" src="'.URL.'public/plugins/'.$js.'"></script>';
    }
}
?>
<!-- Custom Js -->
<script src="<?php echo URL.'public/js/admin.js'?>"></script>
<!-- Demo Js -->
<script src="<?php echo URL.'public/js/demo.js'?>"></script>

<?php
if(isset($this->js)){
    foreach ($this->js as $js){
        echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
    }
}
?>
</body>

</html>
