<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/25/2016
 * Time: 8:45 PM
 */
?>
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="duta-menu fullpage-wrap small">

        <ul class="slides" style="
    width: 100%;">
            <li style="background-image: url(<?php echo URL ?>public/images/banner9.jpg);
                    background-size: 100% 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="kontak flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small white flex-animation no-opacity animated fadeInUp">
                        REFERENSI 3
                    </h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <div class="row margin-leftright-null text-center">
            <div class="col-md-12 padding-leftright-null">
                <div id="sidebar" class="visi-container col-md-2 padding-leftright-null">
                    <div id="sidebar-content" class=" move text text-center padding-right-null padding-md-bottom-null">
                        <ul class="nav nav-pills nav-stacked col-md-12">
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/struktur'?>">
                                    Struktur
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/operasional'?>">
                                    Operasional
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/sistem'?>">
                                    Sistem
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/unsur'?>">
                                    Unsur
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/assets'?>">
                                    Jenis Asset
                                </a>
                            </li>
                            <li id="duta" class="menu active">
                                <a class="active">
                                    Referensi
                                </a>
                                <div class="sub-menu">
                                    <ul class="sub-menu nav nav-pills nav-stacked">
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/1'?>">Referensi 1</a></li>
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/2'?>">Referensi 2</a></li>
                                        <li  class="active"><a href="<?php echo URL.'id/avavadutaindonesia/referensi/3'?>">Referensi 3</a></li>
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/4'?>">Referensi 4</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/legalitas'?>">
                                    Legalitas
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/dokumen'?>">
                                    Dokumen
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo URL.'id/avavadutaindonesia/prestasi'?>">
                                    Prestasi
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/aktifitas'?>">
                                    Aktifitas
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class=" col-md-10  padding-leftright-null">
                    <div class="text">
                        <div class="padding-onlytop-sm">
                            <h1 class="title">Referensi 3</h1>
                            <p class="padding-onlytop-md">BPRS VITKA CENTRA - BATAM
                                <br/>
                                PLAZA TANJUNG PANTUN – BATAM
                                <br/>
                                BTKLPP KEMENKES RI. KELAS 1 – BATAM
                            </p>
                            <div class="col-sm-12 col-md-12">
                                <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">

                                    <div class="text text-center padding-top-null padding-md-bottom-null">
                                        <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/ref4.jpg"
                                             height="240px"
                                             width="100%">
                                    </div>
                                </div>
                                <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">
                                    <div class="text text-center padding-top-null padding-md-bottom-null">
                                        <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/ref5.png"
                                             height="240px"
                                             width="100%">
                                    </div>
                                </div>
                                <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">
                                    <div class="text text-center padding-top-null padding-md-bottom-null">
                                        <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/ref6.jpg"
                                             height="240px"
                                             width="100%">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







