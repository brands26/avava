<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/25/2016
 * Time: 8:47 PM
 */
?>
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="duta-menu fullpage-wrap small">

        <ul class="slides" style="
    width: 100%;">
            <li style="background-image: url(<?php echo URL ?>public/images/banner9.jpg);
                    background-size: 100% 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="kontak flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small white flex-animation no-opacity animated fadeInUp">
                        AKTIVITAS
                    </h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <div class="row margin-leftright-null text-center">
            <div class="col-md-12 padding-leftright-null">
                <div id="sidebar" class="visi-container col-md-2 padding-leftright-null">
                    <div id="sidebar-content" class=" move text text-center padding-right-null padding-md-bottom-null">
                        <ul class="nav nav-pills nav-stacked col-md-12">
                            <li>
                                <a href="<?php echo URL . 'id/avavadutaindonesia/struktur' ?>">
                                    Struktur
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL . 'id/avavadutaindonesia/operasional' ?>">
                                    Operasional
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL . 'id/avavadutaindonesia/sistem' ?>">
                                    Sistem
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL . 'id/avavadutaindonesia/unsur' ?>">
                                    Unsur
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL . 'id/avavadutaindonesia/assets' ?>">
                                    Jenis Asset
                                </a>
                            </li>
                            <li id="duta" class="menu">
                                <a>
                                    Referensi
                                </a>
                                <div class="sub-menu">
                                    <ul class="sub-menu nav nav-pills nav-stacked">
                                        <li><a href="<?php echo URL . 'id/avavadutaindonesia/referensi/1' ?>">Referensi
                                                1</a></li>
                                        <li><a href="<?php echo URL . 'id/avavadutaindonesia/referensi/2' ?>">Referensi
                                                2</a></li>
                                        <li><a href="<?php echo URL . 'id/avavadutaindonesia/referensi/3' ?>">Referensi
                                                3</a></li>
                                        <li><a href="<?php echo URL . 'id/avavadutaindonesia/referensi/4' ?>">Referensi
                                                4</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="<?php echo URL . 'id/avavadutaindonesia/legalitas' ?>">
                                    Legalitas
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL . 'id/avavadutaindonesia/dokumen' ?>">
                                    Dokumen
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL . 'id/avavadutaindonesia/prestasi' ?>">
                                    Prestasi
                                </a>
                            </li>
                            <li class="active">
                                <a href="<?php echo URL . 'id/avavadutaindonesia/aktifitas' ?>">
                                    Aktifitas
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div id="image-list-page" class="col-md-10  padding-leftright-null">
                    <div class="text">
                        <div class="padding-onlytop-sm">
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (Perusahaan)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/aktivitas_per1.jpg"
                                         height="auto" width="100%">
                                    <p>Ucapan Selamat dari Kapolda Kepri pada HUT Satpam ke – 33 Tahun 2013 di Batamindo

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/aktivitas_per2.jpg"
                                         height="auto" width="100%">
                                    <p>
                                        Panitia HUT SATPAM Ke - 33 Thn 2013 ( Bhakti Sosial Donor Darah di Nagoya Hill )


                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (Seminar)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/aktifitas_seminar1.jpg"
                                         height="auto" width="100%">
                                    <p>
                                        Bersama Dirbinmas Baharkam Polri ( Brigjen Pol. Drs. Hengkie Kaluara,Msi ) pada
                                        Seminar Nasional Security di Batam


                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/aktifitas_seminar2.jpg"
                                         height="auto" width="100%">
                                    <p>


                                        Bersama Bpk Satpam Indonesia ( Jend.Pol. (Purn) Prof.Dr. Awaloedin Djamin, MPA )
                                        pada International Security Conference Thn. 2012 di Jakarta


                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (Sertifikasi)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/aktifitas_ser1.jpg"
                                         height="auto" width="100%">
                                    <p>Mengikuti Pelatihan & Sertifikasi GADA UTAMA
                                        ( Green Gadog – Jawa Barat )

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/aktifitas_ser2.jpg"
                                         height="auto" width="100%">
                                    <p>
                                        Peserta GADA UTAMA Angkatan.IV Thn 2014


                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (Pembinaan Mental & Fisik)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/fisik1.jpg"
                                         height="auto" width="100%">
                                    <p>Pembinaan Mental Personil
                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/fisik2.jpg"
                                         height="auto" width="100%">
                                    <p>
                                        Pembinaan Fisik Personil
                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (Perusahaan)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/latihan1.jpg"
                                         height="auto" width="100%">Latihan & Penyegaran Personil Satpam

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/latihan2.jpg"
                                         height="auto" width="100%">
                                    <p>
                                        Latihan Pemadam Kebakaran


                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (Lingkungan)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/lingkungan1.jpg"
                                         height="auto" width="100%">
                                    <p>Kegiatan Penghijauan bersama Dirbinmas Polda Kepri pada HUT Satpam Ke - 33 Thn
                                        2013

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/lingkungan2.jpg"
                                         height="auto" width="100%">
                                    <p>
                                        Kegiatan Penghijauan yg dilaksanakan di Simpang Jam - Batam

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (USER)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/user1.jpg"
                                         height="auto" width="100%">
                                    <p>Mengikuti Rapat di Kantor salah satu USER PT.AVAVA DUTA INDONESIA
                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/user2.jpg"
                                         height="auto" width="100%">
                                    <p>
                                        Mendukung Kegiatan User untuk penilaian PELABUHAN SEHAT INTERNATIONAL Thn 2013
                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (Organisasi / Kemasyarakatan)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/org1.jpg"
                                         height="auto" width="100%">
                                    <p>Membina Hubungan Baik dengan LSM & Organisasi

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/org2.jpg"
                                         height="auto" width="100%">
                                    <p>
                                        Sebagai Pengurus DPD AMSI KEPRI

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null left-align">
                                <h1 class="text margin-bottom-null title">Aktifitas (Sosial)</h1>

                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/sosial1.jpg"
                                         height="auto" width="100%">
                                    <p>Menghadiri Peresmian Yayasan Al-khairath Batam

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/sosial2.jpg"
                                         height="auto" width="100%">
                                    <p>Memberikan Santunan Kepada Salah Satu Yayasan Binaan di Bengkong


                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class=""
                                         src="<?php echo URL; ?>public/images/uploads/sosial3.jpg"
                                         height="auto" width="100%">
                                    <p>Kegiatan Donor Darah

                                    </p>
                                </div>
                            </div>
                            <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                <div class="text text-center padding-top-null padding-md-bottom-null">
                                    <img class="w"
                                         src="<?php echo URL; ?>public/images/uploads/sosial4.jpg"
                                         height="auto" width="100%">
                                    <p>Kegiatan Korban Idul Adha 1434.H


                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



