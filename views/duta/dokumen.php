<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/25/2016
 * Time: 8:46 PM
 */
?>
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="duta-menu fullpage-wrap small">

        <ul class="slides" style="
    width: 100%;">
            <li style="background-image: url(<?php echo URL ?>public/images/banner9.jpg);
                    background-size: 100% 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="kontak flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small white flex-animation no-opacity animated fadeInUp">
                        DOKUMEN
                    </h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <div class="row margin-leftright-null text-center">
            <div class="col-md-12 padding-leftright-null">
                <div id="sidebar" class="visi-container col-md-2 padding-leftright-null">
                    <div id="sidebar-content" class="text text-center padding-right-null padding-md-bottom-null">
                        <ul class="nav nav-pills nav-stacked col-md-12">
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/struktur'?>">
                                    Struktur
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/operasional'?>">
                                    Operasional
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/sistem'?>">
                                    Sistem
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/unsur'?>">
                                    Unsur
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/assets'?>">
                                    Jenis Asset
                                </a>
                            </li>
                            <li id="duta" class="menu">
                                <a >
                                    Referensi
                                </a>
                                <div class="sub-menu">
                                    <ul class="sub-menu nav nav-pills nav-stacked">
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/1'?>">Referensi 1</a></li>
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/2'?>">Referensi 2</a></li>
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/3'?>">Referensi 3</a></li>
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/4'?>">Referensi 4</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/legalitas'?>">
                                    Legalitas
                                </a>
                            </li>
                            <li class="active">
                                <a href="<?php echo URL.'id/avavadutaindonesia/dokumen'?>">
                                    Dokumen
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo URL.'id/avavadutaindonesia/prestasi'?>">
                                    Prestasi
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/aktifitas'?>">
                                    Aktifitas
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div id="image-list-dokumen" class="col-md-10  padding-leftright-null">
                    <div class="text">
                        <div class="padding-onlytop-sm">
                            <div class="col-md-12 padding-leftright-null text-center">

                                <div class="padding-md">
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                        <h1 class=" text margin-bottom-null title">KEPUTUSAN MENKUMHAM
                                        </h1>
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/keputusan_menkum.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                        <h1 class="text margin-bottom-null title">AKTA PENDIRIAN PERUSAHAAN
                                        </h1>
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/akta_pendirian.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding-leftright-null text-center">

                                <div class="padding-md">

                                    <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null">
                                        <h1 class="text margin-bottom-null title">IZIN KEPOLISIAN</h1>

                                    </div>
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/izin_kepolisian1.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/izin_kepolisian2.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding-leftright-null text-center">

                                <div class="padding-md">

                                    <div id="" class="visi-container col-sm-12 col-md-12 padding-leftright-null">
                                        <h1 class="text margin-bottom-null title">SIOJPTK</h1>

                                    </div>
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/siojptk1.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/siojptk2.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding-leftright-null text-center">

                                <div class="padding-md">
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                        <h1 class="text margin-bottom-null title">SIUP PERUSAHAAN
                                        </h1>
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/siup.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                        <h1 class="text margin-bottom-null title">DOMISILI PERUSAHAAN
                                        </h1>
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/domisili.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding-leftright-null text-center">

                                <div class="padding-md">
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                        <h1 class="text margin-bottom-null title">PENGHARGAAN
                                            DARI POLDA KEPRI THN 2012
                                        </h1>
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/penghargaan2012.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                        <h1 class="text margin-bottom-null title">PENGHARGAAN
                                            DARI POLDA KEPRI THN 2013
                                        </h1>
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/penghargaan2013.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 padding-leftright-null text-center">

                                <div class="padding-md">
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                                        <h1 class="text margin-bottom-null title">SERTIFIKASI
                                            GADA UTAMA MABES POLRI
                                        </h1>
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/gada_utama.jpg"
                                                 height="600px" width="100%">
                                        </div>
                                    </div>
                                    <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                                        <h1 class="text margin-bottom-null title">SURAT TANDA ANGGOTA
                                            AMSI
                                        </h1>
                                        <div class="text text-center padding-top-null padding-md-bottom-null">
                                            <img class=""
                                                 src="<?php echo URL; ?>public/images/uploads/amsi.jpg"
                                                 height="auto" width="100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



