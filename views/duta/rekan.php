<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/27/2016
 * Time: 8:07 AM
 */
?>

<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider-nav" class="h450 fullpage-wrap small">

        <div class="slider-navigation">
            <a href="#" class="flex-prev"><i class="material-icons">keyboard_arrow_left</i></a>
            <div class="slider-controls-container"></div>
            <a href="#" class="flex-next"><i class="material-icons">keyboard_arrow_right</i></a>
        </div>
        <ul class="slides">
            <?php
            if (isset($this->rekan[0]->logo_rekanan)) {
                $datas = explode(',', $this->rekan[0]->logo_rekanan);
                foreach ($datas as $data) {
                    ?>
                    <li style="background-image: url(<?php echo URL . 'public/images/uploads/' . ($data!=''?$data:'1479860747.jpg') ?>);"
                        class="clone" aria-hidden="true">
                        <div class="text">
                            <h1 class="white flex-animation no-opacity animated fadeInUp"><?php echo $this->rekan[0]->nama_rekanan; ?></h1>
                        </div>
                        <div class="gradient dark"></div>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <div class="row margin-leftright-null text-center">
            <div class="col-md-12 padding-leftright-null">
                <div class="text">
                    <div class="heading padding-onlytop-sm heading center">
                        <?php echo($this->menus['lang'] == 'id' ? ($this->rekan[0]->konten_rekanan != null ? $this->rekan[0]->konten_rekanan : $this->rekan[0]->konten_en_rekanan) : ($this->rekan[0]->konten_en_rekanan != null ? $this->rekan[0]->konten_en_rekanan : $this->rekan[0]->konten_rekanan)); ?>
                    </div>
                    <p class="text">
                    <h3>Website</h3><a
                            href="<?php echo($this->rekan[0]->web_rekanan != null? $this->rekan[0]->web_rekanan : '') ?>"><?php echo($this->rekan[0]->web_rekanan != null || $this->rekan[0]->web_rekanan != '' ? $this->rekan[0]->web_rekanan : '-') ?></a></p>
                </div>
            </div>
        </div>
    </div>
</div>

