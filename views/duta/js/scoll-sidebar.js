/**
 * Created by Brands on 11/26/2016.
 */

$(window).scroll(function (event) {
    if ($(window).width() > 480) {

        var scroll = $(window).scrollTop();
        var backs = $("#home-wrap");
        var content = $("#sidebar-content");
        var start = $('#flexslider').height()-50;
        var tot_content = (backs.height() - content.height())+250;
        console.log(start);
        if (scroll > start && scroll < tot_content) {
            content.addClass('move');
            content.css("top", (scroll - start));
        }
    }

});