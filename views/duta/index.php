<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 24/11/2016
 * Time: 20:15
 */
?>
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="duta-menu fullpage-wrap small">
        <ul class="slides" style="width: 100%">
            <li style="background-image: url(<?php echo URL ?>public/images/banner9.jpg); background-size: 100% 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small flex-animation no-opacity animated fadeInUp">PT. AVAVA DUTA
                        INDONESIA</h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <!-- Services -->
        <div class="row no-margin">
            <div class="col-md-12 padding-leftright-null">
                <div class="col-md-12 padding-leftright-null">


                    <div id="sidebar" class="visi-container col-sm-12 col-md-2 padding-leftright-null">
                        <div id="sidebar-content" class="text text-center padding-right-null padding-md-bottom-null">
                            <ul class="nav nav-pills nav-stacked col-md-12">
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/struktur' ?>">
                                        Struktur
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/operasional' ?>">
                                        Operasional
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/sistem' ?>">
                                        Sistem
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/unsur' ?>">
                                        Unsur
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/assets' ?>">
                                        Jenis Asset
                                    </a>
                                </li>
                                <li id="duta" class="menu">
                                    <a>
                                        Referensi
                                    </a>
                                    <div class="sub-menu">
                                        <ul class="sub-menu nav nav-pills nav-stacked">
                                            <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/1'?>">Referensi 1</a></li>
                                            <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/2'?>">Referensi 2</a></li>
                                            <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/3'?>">Referensi 3</a></li>
                                            <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/4'?>">Referensi 4</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/legalitas' ?>">
                                        Legalitas
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/dokumen' ?>">
                                        Dokumen
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/prestasi' ?>">
                                        Prestasi
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL . 'id/avavadutaindonesia/aktifitas' ?>">
                                        Aktifitas
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div id="" class="col-sm-12 col-md-10 padding-leftright-null">
                        <div class="col-md-12 padding-leftright-null">
                            <div class="text text-center padding-bottom-null">
                                <img src="<?php echo URL; ?>public/images/logo_duta.png" height="auto" width="200px">
                                <p>Timbulnya permasalahan dengan meningkatnya kualitas dan kuantitas aksi kriminal di
                                    Indonesia serta kondisi jumlah aparat Penegak Hukum yang tidak seimbang jika
                                    dibandingkan dengan jumlah penduduk, mengakibatkan kecenderungan banyaknya
                                    permasalahan hukum yang tidak dilaporkan, dan juga yang tidak dapat terselesaikan
                                    dengan secara optimal.</p>
                            </div>
                        </div>
                        <div class="col-md-12 padding-leftright-null">
                            <div class="text text-center padding-null">
                                <h1 class="margin-bottom-null title">PT. AVAVA DUTA INDONESIA </h1>
                            </div>
                        </div>
                        <div class="col-md-12  padding-leftright-null">
                            <div class="text text-center padding-bottom-null">
                                <p>merupakan sebuah Badan Usaha Jasa Pengamanan yang hadir untuk memberikan jasa
                                    pelayanan keamanan dan pengamanan, menyelamatkan dan melindungi asset perusahaan
                                    maupun perorangan, kekayaan, harta benda, serta martabat dan kehormatan serta
                                    kepentingan bisnis para pelanggan.
                                    <br/>
                                    Dengan kehadiran kami ( PT. AVAVA DUTA INDONESIA ), bertujuan untuk berupaya
                                    membantu Aparat Keamanaan untuk mewujudkan Kamtibmas yang selanjutnya akan mendukung
                                    Stabilitas Keamanan dan kelancaran roda perekonomian.
                                </p>
                            </div>
                        </div>
                        <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">

                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <h1 class=" title">Visi </h1>
                                <img src="<?php echo URL; ?>public/images/hand.png" height="80px" width="80px">
                                <h6 class="medium dark"></h6>
                                <p class="margin-md-bottom-null">Menjadi sebuah Badan Usaha Jasa Pengamanan yang mampu
                                    membantu dan bekerja sama dengan aparat pemerintah, swasta dan perorangan dalam
                                    mengamankan dan menyelamatkan harta kekayaan yang menjadi asset Pengguna.
                                </p>
                            </div>
                        </div>
                        <div id="" class="visi-container col-sm-12 col-md-6 padding-leftright-null">
                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <h1 class=" title">Misi </h1>
                                <img src="<?php echo URL; ?>public/images/secure.png" height="80px" width="80px">
                                <h6 class="medium dark"></h6>
                                <p class="margin-md-bottom-null">Membantu aparat pemerintah, swasta dan perorangan dalam
                                    rangka meminimalisir permasalahan yang dihadapi melalui proses Sistem Manajemen
                                    Resiko, dengan memberikan jasa pengamanan, penyelamatan, konsultasi, verifikasi dan
                                    auditori dalam rangka pembuktian kasus-kasus yang melawan hukum dengan batasan pada
                                    aspek legal, efektif dan efisien.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Services -->
        <div class="row margin-leftright-null grey-background text">
            <div class="col-md-12 padding-leftright-null text-center">
                <h1 class=" title ">Sertifikasi & SDM</h1>

                <div class="col-md-12 padding-leftright-null text-center">
                    <div class="col-md-6 text-center">
                        <h2 class=" title nobar">Sertifikasi </h2>
                        <p>
                            dari Kepolisian Negara .RI Personil Siap Menjalankan Tugas.
                        </p>
                    </div>
                    <div class="col-md-6 text-center">
                        <h2 class=" title nobar">Arahan direktur </h2>
                        <p>
                            Para Anggota yang Akan Bertugas Mendapat Arahan dari Direktur Sebelum diberangkatkan ke
                            Lokasi Tugas yang Baru
                        </p>
                    </div>

                </div>
                <div class="padding-md">
                    <h2 class=" title nobar">
                        SUMBER DAYA MANUSIA
                    </h2>
                    <p>SDM merupakan faktor penting dalam usaha jasa dan merupakan perhatian utama dari pelanggan.
                        Perusahaan ini dikelolah oleh perpaduan dari para mantan anggota Aparat Keamanan dan sipil
                        terlatih dengan tenaga Professional yang telah berpengalaman dalam melaksanakan tugas-tugas,
                        verifikasi dokumen, mengaudit data dan merekayasa sistim pengamanan, serta memiliki akses yang
                        luas dengan Lembaga/Institusi Pemerintah diantaranya dengan aparat penegak hukum, khususnya
                        Kepolisian.</p>
                    <p>Sebagai perusahaan dibidang jasa keamanan dan perlindungan, tugas dan produk utama kami adalah
                        menyediakan manusia-manusia yang tangguh dengan suatu persyaratan dasar dimana kemampuan
                        dibidang keamanan, disiplin dan profesionalisme mereka adalah yang terbaik. Sebagian besar calon
                        anggota keamanan kami diambil dari orang-orang lokal yang berada di dalam suatu area proyek
                        terkait.</p>
                </div>
            </div>
            <div id="" class="visi-container col-sm-12 col-md-4 col-md-push-2 padding-leftright-null">

                <div class="text text-center padding-top-null padding-md-bottom-null">
                    <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/sertifikasi.jpg"
                         height="240px" width="100%">
                </div>
            </div>
            <div id="" class="visi-container col-sm-12 col-md-4 col-md-push-2 padding-leftright-null">
                <div class="text text-center padding-top-null padding-md-bottom-null">
                    <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/sertifikasi2.jpg"
                         height="240px" width="100%">
                </div>
            </div>
        </div>

        <div class="row margin-leftright-null text">
            <div class="col-md-12 padding-leftright-null text-center">
                <h1 class="margin-bottom-null title">Manfaat</h1>

                <div class="padding-md">
                    <div class="col-md-12 padding-leftright-null text-center">
                        <div class="col-md-3 text-center">
                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <img src="<?php echo URL; ?>public/images/pengalaman_logo.png" height="80"
                                     width="80px">
                            </div>
                            <p>
                                Pengalaman ditangani dan dikelola secara profesional karena kami sebagai pihak yang
                                memborong
                                pekerjaan pengamanan tetap mengupayakan optimalisasi mutu pelayanan dan kepercayaan
                                pengguna.
                            </p>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <img src="<?php echo URL; ?>public/images/access_logo.png" height="80"
                                     width="80px">
                            </div>
                            <p>
                                Memiliki akses dan hubungan baik kepada instansi pemerintah, lembaga, badan, organisasi
                                swasta.
                            </p>
                        </div>

                        <div class="col-md-3 text-center">
                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <img src="<?php echo URL; ?>public/images/good_logo.png" height="80"
                                     width="80px">
                            </div>

                            <p>
                                Memiliki akses dan hubungan baik kepada instansi pemerintah, lembaga, badan, organisasi
                                swasta.
                            </p>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <img src="<?php echo URL; ?>public/images/pengeluaran_logo.png" height="80"
                                     width="80px">
                            </div>
                            <p>
                                Mengurangi terjadinya pengeluaran biaya / anggaran perusahaan yang bersifat Non
                                Budgeter.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-12 padding-leftright-null text-center">
                        <div class="col-md-3 col-md-push-1  text-center">
                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <img src="<?php echo URL; ?>public/images/fokus_logo.png" height="80"
                                     width="80px">
                            </div>
                            <p class="margin-md-bottom-null">
                                Proses mencapaian target perusahaan lebih fokus pada tujuan intinya.
                            </p>
                        </div>
                        <div class="col-md-4 col-md-push-1 text-center">
                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <img src="<?php echo URL; ?>public/images/konflik_logo.png" height="80"
                                     width="80px">
                            </div>
                            <p class="margin-md-bottom-null">
                                Pada kondisi emergensi seperti Demo karyawan, konflik internal maupun eksternal atau
                                perselisian
                                dengan pihak lain, kami dapat selalu memberikan proteck dan perlindungan terhadap
                                perusahaan
                                berikut asset yang ada.
                            </p>
                        </div>
                        <div class="col-md-3 col-md-push-1 text-center">
                            <div class="text text-center padding-top-null padding-md-bottom-null">
                                <img src="<?php echo URL; ?>public/images/perekrutan_logo.png" height="80"
                                     width="80px">
                            </div>
                            <p class="margin-md-bottom-null">
                                Mengurangi problem administrasi User, seperti : Perekrutan, Pengelolaan SDM personil
                                security,
                                Keuangan, kenyamanan / kosentrasi kerja meningkat.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row margin-leftright-null grey-background text">
            <div class="col-md-12 padding-leftright-null text-center">
                <h1 class="margin-bottom-null title">Pembekalan <br/> Portable Fire Extinguisher</h1>

                <div class="padding-md">
                    <div id="" class="visi-container col-sm-12 col-md-4 col-md-push-2 padding-leftright-null">

                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/pembekalan1.jpg"
                                 height="240px"
                                 width="100%">
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-4 col-md-push-2 padding-leftright-null">
                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/pembekalan2.jpg"
                                 height="240px"
                                 width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-leftright-null text">
            <div class="col-md-12 padding-leftright-null text-center">
                <h1 class="margin-bottom-null title">Briefing & Pembinaan</h1>

                <div class="padding-md">
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">

                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/brief1.jpg"
                                 height="240px" width="100%">
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">
                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/brief2.jpg"
                                 height="240px" width="100%">
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">
                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/brief3.jpg"
                                 height="240px" width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row margin-leftright-null grey-background text">
            <div class="col-md-12 padding-leftright-null text-center">
                <h1 class="margin-bottom-null title">Memberikan Pelayanan Pengamanan</h1>

                <div class="padding-md">
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">

                        <div class="padding-md text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/pelayanan1.jpg"
                                 height="240px"
                                 width="100%">
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">
                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/pelayanan2.jpg"
                                 height="240px"
                                 width="100%">
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">
                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/pelayanan3.jpg"
                                 height="240px"
                                 width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-leftright-null text">
            <div class="col-md-12 padding-leftright-null text-center">
                <h1 class="margin-bottom-null title">Kemampuan Tambahan</h1>

                <div class="padding-md">
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">

                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/kemampuan1.jpg"
                                 height="240px"
                                 width="100%">
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">
                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/kemampuan2.jpg"
                                 height="240px"
                                 width="100%">
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-4 padding-leftright-null">
                        <div class="text text-center padding-top-null padding-md-bottom-null">
                            <img class="withBorder" src="<?php echo URL; ?>public/images/uploads/kemampuan3.jpg"
                                 height="240px"
                                 width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row margin-leftright-null grey-background text">
            <div class="col-md-12  padding-leftright-null text-center">
                <h1 class="margin-bottom-null title">Rekanan</h1>
                <div class="padding-md">
                    <?php $jumlah = count($this->rekanan);
                    $tot_row = ceil($jumlah / 3);
                    $row = 0;
                    for ($col = 0; $col < 3; $col++) {
                        ?>
                        <div class="col-sm-12 col-md-4">
                            <div id="tableRekanan" class="">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th>#</th>
                                        <th>Perusahaan</th>
                                        <th>Status</th>
                                    </tr>

                                    <?php
                                    for ($a = 0; $a < $tot_row; $a++) {
                                        if (isset($this->rekanan[$row])) {
                                            ?>
                                            <tr>
                                                <td><?php echo $row + 1; ?></td>
                                                <td>
                                                    <a href="<?php echo URL . $this->menus['lang'] . '/avavadutaindonesia/rekan/' . strtolower($this->rekanan[$row]->url_rekanan) ?>"><?php echo $this->rekanan[$row]->nama_rekanan ?></a>
                                                </td>
                                                <td><?php echo($this->rekanan[$row]->status_rekanan == 1 ? 'Masih Dalam Kontrak' : 'Selesai Kontrak') ?></td>
                                            </tr>
                                            <?php
                                            $row +=1;
                                        }
                                    } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row margin-leftright-null text">
            <div class="col-md-12  padding-leftright-null text-center">
                <h1 class=" title">penutup</h1>

                <div class="col-md-12 padding-top-lg padding-leftright-null text-center">
                    <div class="col-md-4 padding-leftright-null text-center">
                        <img src="<?php echo URL; ?>public/images/uploads/logo3.png" height="120px" width="120px">
                    </div>
                    <div class="col-md-4 padding-leftright-null text-center">
                        <img src="<?php echo URL; ?>public/images/uploads/logo1.png" height="120px" width="120px">
                    </div>
                    <div class="col-md-4 padding-leftright-null text-center">
                        <img src="<?php echo URL; ?>public/images/uploads/logo2.png" height="120px" width="120px">
                    </div>
                </div>
                <div class="padding-md">
                    <p class="margin-md-bottom-null">
                        Bertumpu pada semangat dan tekad untuk menjadi lebih baik dan lebih baik lagi, PT.AVAVA DUTA
                        INDONESIA memfokuskan segenap sumber daya yang dimiliki untuk memastikan dihasilkannya Sumber
                        Daya
                        Manusia ( SDM ) yang sesuai dengan tuntutan dan profesionalisme yang dikehendaki. Dalam
                        mengemban
                        tugas dan tanggung jawab pengamanan pribadi maupun asset perusahaan kami senantiasa berpegang
                        teguh
                        pada nilai-nilai ( value ) yang menjadi kekuatan kami yakni :
                        <br/>
                        <br/>
                        PROFESIONAL, CERDAS, BERWIBAWA, SMART, KOMITMEN YANG TINGGI, SELALU TERDEPAN, BERDEDIKASI, KERJA
                        SAMA, KWALITAS PELAYANAN, INTEGRITAS, DAPAT DIPERCAYA DAN KOMUNIKATIF.
                        <br/>
                        <br/>
                        PT.AVAVA DUTA INDONESIA berbagi tanggung jawab professional baik kepada para mitra usaha dalam
                        mengelola pembangunan bangsa pada umumnya dan pembangunan ekonomi kerakyatan pada khususnya demi
                        kesejahteraan kita bersama. Lebih dari itu, kami ingin berbagi tanggung jawab dalam menjaga
                        keamanan
                        dan kebersihan dengan mitra usaha untuk turut serta mendukung kami.
                    </p>
                    <h3>“ KEPUASAN PELANGGAN ADALAH KESUKSESAN KAMI ”
                    </h3>


                </div>
            </div>
        </div>
    </div>
</div>
