<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/25/2016
 * Time: 8:46 PM
 */
?>
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="duta-menu fullpage-wrap small">

        <ul class="slides" style="
    width: 100%;">
            <li style="background-image: url(<?php echo URL ?>public/images/banner9.jpg);
                    background-size: 100% 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="kontak flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small white flex-animation no-opacity animated fadeInUp">
                        LEGALITAS
                    </h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <div class="row margin-leftright-null text-center">
            <div class="col-md-12 padding-leftright-null">
                <div id="sidebar" class="visi-container col-md-2 padding-leftright-null">
                    <div id="sidebar-content" class=" move text text-center padding-right-null padding-md-bottom-null">
                        <ul class="nav nav-pills nav-stacked col-md-12">
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/struktur'?>">
                                    Struktur
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/operasional'?>">
                                    Operasional
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/sistem'?>">
                                    Sistem
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/unsur'?>">
                                    Unsur
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/assets'?>">
                                    Jenis Asset
                                </a>
                            </li>
                            <li id="duta" class="menu">
                                <a >
                                    Referensi
                                </a>
                                <div class="sub-menu">
                                    <ul class="sub-menu nav nav-pills nav-stacked">
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/1'?>">Referensi 1</a></li>
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/2'?>">Referensi 2</a></li>
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/3'?>">Referensi 3</a></li>
                                        <li><a href="<?php echo URL.'id/avavadutaindonesia/referensi/4'?>">Referensi 4</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="active">
                                <a href="<?php echo URL.'id/avavadutaindonesia/legalitas'?>">
                                    Legalitas
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/dokumen'?>">
                                    Dokumen
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo URL.'id/avavadutaindonesia/prestasi'?>">
                                    Prestasi
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.'id/avavadutaindonesia/aktifitas'?>">
                                    Aktifitas
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-10  padding-leftright-null">
                    <div class="text">
                        <div class="padding-onlytop-sm">

                            <div class="">
                                <table class="">
                                    <tbody>
                                    <tr>
                                        <th>#</th>
                                        <th>
                                            Surat
                                        </th>
                                        <th>No</th>
                                    </tr>
                                    <tr>
                                        <td>1.</td>
                                        <td>AKTE PENDIRIAN PERUSAHAAN
                                        </td>
                                        <td>NO : 34 ( TIGA PULUH EMPAT )
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>SURAT KEPUTUSAN MENKUMHAM

                                        </td>
                                        <td>NO : AHU-02616.AH.01.01 TAHUN 2012
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>SURAT DOMISILI PERUSAHAAN
                                        </td>
                                        <td>NO : 1021/DOM/517/LB/XII/2011
                                        </td>
                                    </tr><tr>
                                        <td>4.</td>
                                        <td>NPWP</td>
                                        <td>NO : 03.198.000.6.215.000
                                        </td>
                                    </tr><tr>
                                        <td>5.</td>
                                        <td>TANDA DAFTAR PERUSAHAAN
                                        </td>
                                        <td>NO : 33.10.1.78.10609
                                        </td>
                                    </tr><tr>
                                        <td>6.</td>
                                        <td>SURAT IZIN USAHA PERDAGANGAN
                                        </td>
                                        <td>NO : 00281/BPM-BTM/PK/III/2013
                                        </td>
                                    </tr><tr>
                                        <td>7.</td>
                                        <td>DISNAKERTRAN BATAM ( LPTKS )
                                        </td>
                                        <td>NO : KEP.86/TK-2/II/2012
                                        </td>
                                    </tr><tr>
                                        <td>8.</td>
                                        <td>DINASKERTRAN KEPRI (SIOJPTK)
                                        </td>
                                        <td>NO : 36/DTKT-JTK/III/2013
                                        </td>
                                    </tr><tr>
                                        <td>9.</td>
                                        <td>JAMSOSTEK
                                        </td>
                                        <td>NO : NPP:DD025374
                                        </td>
                                    </tr><tr>
                                        <td>10.</td>
                                        <td>SURAT IZIN OPERASIONAL MABES
                                        </td>
                                        <td>NO : SI/4863/VI/2013
                                        </td>
                                    </tr><tr>
                                        <td>11.</td>
                                        <td>KARTU KEANGGOTAAN AMSI
                                        </td>
                                        <td>NO :STK-017/AMSI KEPRI/IV/2013
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



