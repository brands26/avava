<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="fullpage-wrap small">
        <ul class="slides">
            <li style="background-image: url(<?php echo URL ?>public/images/banner7.jpg); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small white flex-animation no-opacity animated fadeInUp"><?php echo $this->menus['event']?></h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap grey-background row">
        <div class="col-md-9 padding-leftright-null">
            <!--  News Section  -->
            <section id="news" class="page">
                <div class="news-items equal one-columns" style="position: relative; height: 1088px;">
                    <?php
                    foreach ($this->Event->data as $event) {
                        ?>
                        <div class="single-news one-item horizontal-news berita"
                             style="position: absolute; left: 0px; top: 0px;">
                            <article>
                                <div class="col-md-6 padding-leftright-null">
                                    <div class="image"
                                         style="background-image:url(<?php echo $event->image_post; ?>)"></div>
                                </div>
                                <div class="col-md-6 padding-leftright-null">
                                    <div class="content">
                                                <span class="read">
                                                   Read More
                                                </span>
                                        <h3><?php echo $event->title_post; ?></h3>
                                        <span class="date"><?php echo $event->created_post; ?></span>
                                        <p><?php echo substr(strip_tags($event->content_post), 0, 200); ?></p>
                                    </div>
                                </div>
                                <a href="<?php echo URL . $this->menus['lang'] . '/event/post/'.$event->url_post ?>" class="link"></a>
                            </article>
                        </div>
                        <?php
                    } ?>
                </div>
            </section>
            <!--  END News Section  -->
            <!--  Navigation  -->
            <section id="nav" class="padding-top-null grey-background">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="nav-left">
                            <?php if ($this->Event->back) { ?>
                                <a href="<?php echo URL.$this->menus['lang'].'/event'.($this->Event->search?'/search/'.$this->Event->keyword.'/':'/page/').$this->Event->backpage ?>" class="btn-alt small active shadow margin-null"><i
                                        class="icon ion-ios-arrow-left"></i><span>Older posts</span></a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="nav-right">
                            <?php if ($this->Event->next) { ?>
                                <a href="<?php echo URL.$this->menus['lang'].'/event'.($this->Event->search?'/search/'.$this->Event->keyword.'/':'/page/').$this->Event->nextpage?>" class="btn-alt small active shadow margin-null"><span>Newer posts</span><i
                                        class="icon ion-ios-arrow-right"></i></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </section>
            <!--  END Navigation  -->
        </div>
        <!--  Right Sidebar  -->
        <div class="col-md-3 text">
            <aside class="sidebar">
                <div class="widget-wrapper">
                    <form id="search-form" class="search-form" action="">
                        <div class="form-input">
                            <input id="search-input" data-href="<?php echo URL.$this->menus['lang'].'/event/search/'?>" type="text" placeholder="Search...">
                            <span class="form-button">
                                            <button id="button-search" type="button" >
                                                <i class="icon ion-ios-search-strong"></i>
                                            </button>
                                        </span>
                        </div>
                    </form>
                </div>
                <div class="widget-wrapper">
                    <h5>Recent Post</h5>
                    <ul class="recent-posts">
                        <?php foreach ($this->recentEvent as $recent){
                            ?>

                            <li>
                                <a href="<?php $recent?>">
                                            <span class="meta">
                                                <?php echo $recent->created_post; ?>
                                            </span>
                                    <p><?php echo $recent->title_post; ?></p>
                                </a>
                            </li>
                            <?php
                        }?>
                    </ul>
                </div>
            </aside>
        </div>
        <!--  END Right Sidebar  -->
    </div>
</div>