<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider-nav" class="fullpage-wrap small">

        <div class="slider-navigation">
            <a href="#" class="flex-prev"><i class="material-icons">keyboard_arrow_left</i></a>
            <div class="slider-controls-container"></div>
            <a href="#" class="flex-next"><i class="material-icons">keyboard_arrow_right</i></a>
        </div>
        <ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(-2522px, 0px, 0px);">
            <li style="background-image: url(<?php echo URL ?>public/images/banner1.jpg); width: 1261px; float: left; display: block;"
                class="clone" aria-hidden="true">
                <div class="text">
                    <h1 class="white flex-animation no-opacity animated fadeInUp">Avava International Group</h1>
                    <h2 class="white flex-animation no-opacity animated fadeInUp"> A Company That Provides Solutions
                        Appropriate, Effective, Efficient And Innovative, Also Have A Sensitivity To Social
                        Activities In The Community.</h2>
                </div>
                <div class="gradient dark"></div>
            </li>
            <li style="background-image: url(<?php echo URL ?>public/images/banner2.jpg); width: 1261px; float: left; display: block;"
                class="flex-active-slide">
                <div class="text">
                    <h1 class="white flex-animation no-opacity animated fadeInUp">Avava International Group</h1>
                    <h2 class="white flex-animation no-opacity animated fadeInUp"> A Company That Provides Solutions
                        Appropriate, Effective, Efficient And Innovative, Also Have A Sensitivity To Social
                        Activities In The Community.</h2>
                </div>
                <div class="gradient dark"></div>
            </li>
            <li style="background-image: url(<?php echo URL ?>public/images/banner3.jpg); width: 1261px; float: left; display: block;"
                class="flex-active-slide">
                <div class="text">
                    <h1 class="white flex-animation no-opacity animated fadeInUp">Avava International Group</h1>
                    <h2 class="white flex-animation no-opacity animated fadeInUp"> A Company That Provides Solutions
                        Appropriate, Effective, Efficient And Innovative, Also Have A Sensitivity To Social
                        Activities In The Community.</h2>
                </div>
                <div class="gradient dark"></div>
            </li>
            <li style="background-image: url(<?php echo URL ?>public/images/banner4.jpg); width: 1261px; float: left; display: block;"
                class="flex-active-slide">
                <div class="text">
                    <h1 class="white flex-animation no-opacity animated fadeInUp">Avava International Group</h1>
                    <h2 class="white flex-animation no-opacity animated fadeInUp"> A Company That Provides Solutions
                        Appropriate, Effective, Efficient And Innovative, Also Have A Sensitivity To Social
                        Activities In The Community.</h2>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <!-- Services -->
        <div class="row no-margin">
            <div class="col-md-12 padding-leftright-null">
                <div class="col-md-12 padding-leftright-null">
                    <div class="text text-center padding-bottom-null">
                        <h1 class="margin-bottom-null title"><?php echo ($this->menus['lang'] == 'id'?'VISI PERUSAHAAN':'COMPANY VISION')?></h1>
                    </div>
                </div>
                <div class="col-md-12 padding-leftright-null">
                    <div id="" class="visi-container col-sm-12 col-md-3 padding-leftright-null">
                        <div class="text text-center padding-md-bottom-null">
                            <img src="<?php echo URL; ?>public/images/mitra.png" height="80px" width="80px">
                            <h6 class="medium dark"></h6>
                            <p class="margin-md-bottom-null"><?php echo ($this->menus['lang'] == 'id'?$this->setting->visi_one:$this->setting->visi_one_en)?></p>
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-3 padding-leftright-null">
                        <div class="text text-center padding-md-bottom-null">
                            <img src="<?php echo URL; ?>public/images/perhatian.png" height="80px" width="80px">
                            <h6 class="medium dark"></h6>
                            <p class="margin-md-bottom-null"><?php echo ($this->menus['lang'] == 'id'?$this->setting->visi_two:$this->setting->visi_two_en)?></p>
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-3 padding-leftright-null">
                        <div class="text text-center">
                            <img src="<?php echo URL; ?>public/images/bisnis.png" height="80px" width="80px">
                            <h6 class="medium dark"></h6>
                            <p class="margin-md-bottom-null"><?php echo ($this->menus['lang'] == 'id'?$this->setting->visi_three:$this->setting->visi_three_en)?></p>
                        </div>
                    </div>
                    <div id="" class="visi-container col-sm-12 col-md-3 padding-leftright-null">
                        <div class="text text-center">
                            <img src="<?php echo URL; ?>public/images/meningkat.png" height="80px" width="80px">
                            <h6 class="medium dark"></h6>
                            <p class="margin-md-bottom-null"><?php echo ($this->menus['lang'] == 'id'?$this->setting->visi_four:$this->setting->visi_four_en)?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Services -->
        <div class="row margin-leftright-null grey-background text">
            <div class="col-md-12 padding-leftright-null text-center">
                <h1 class="margin-bottom-null title">Group Companies</h1>

                <div class="padding-md">
                </div>
            </div>
            <div class="tabbable">
                <div class="col-md-4 padding-leftright-null ">
                    <ul class="nav nav-pills nav-stacked col-md-12">
                        <?php
                        $jumlahKategori = sizeof($this->listKategoriGroup);
                        for ($a = 0; $a < $jumlahKategori; $a++) {
                            ?>
                            <li <?php echo($a == 0 ? 'class="active"' : '') ?>><a href="#<?php echo $a ?>"
                                                                                  data-toggle="tab"><?php echo $this->listKategoriGroup[$a]->name_group_kategori; ?></a>
                            </li>
                            <?php
                        } ?>

                    </ul>
                </div>
                <div class="col-md-8 padding-leftright-null" id="news">
                    <div class="tab-content col-md-12 padding-leftright-null ">
                        <?php
                        for ($a = 0; $a < $jumlahKategori; $a++) {
                            ?>

                            <div class="padding-0 tab-pane <?php echo($a == 0 ? 'active' : '') ?> col-sm-12 single-news "
                                 id="<?php echo $a ?>">
                                <article class="text">
                                    <div class="content">
                                        <?php if (isset($this->listGroup[$a]) ) {
                                            $jumlahGroup = sizeof($this->listGroup[$a]);
                                            $jumlahRow = ceil($jumlahGroup/6);
                                            $num =0;
                                            for($i=0;$i<$jumlahRow;$i++){
                                                echo '<ul class="nav nav-pills nav-stacked col-md-6">';
                                                for($u=0;$u<6;$u++){
                                                    if(isset($this->listGroup[$a][$num])){
                                                        echo'<li><a href="'.URL.$this->menus['lang'].'/group/detail/'.$this->listGroup[$a][$num]->url_group.'"><p>'.$this->listGroup[$a][$num]->name_group.'</p></a></li>';
                                                    }
                                                    $num+=1;
                                                }
                                                echo '</ul>';
                                            }
                                        }
                                        ?>

                                    </div>
                                </article>
                            </div>
                            <?php
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <!--  Latest News  -->
        <section class="call-to-action ribbon-wrap" id="ribbon"
                 style="background-image:url(<?php echo URL?>public/images/867s1.jpg);" role="region"
                 aria-label="Ribbon">
            <div class="section-overlay-layer">

                <div id="news" class="text ">
                    <div class="row padding-md-bottom">
                        <div class="col-md-12 padding-leftright-null text-center">
                            <h1 class="margin-bottom-null title"><?php echo ($this->menus['lang'] == 'id'?'Berita Terbaru':'Latest News')?></h1>

                            <div class="padding-md">
                            </div>
                        </div>
                        <?php foreach($this->recentNews as $news){
                            ?>
                            <div class="col-md-4 single-news">
                                <article class="news">
                                    <img src="<?php echo URL.'public/images/uploads/'.$news->image_post ?>" alt="">
                                    <div class="content">
                                        <span class="read">
                                            Read more
                                        </span>
                                        <h3><?php echo $news->title_post ?>
                                        </h3>
                                        <span class="date"><?php echo $news->created_post ?></span>
                                        <p><?php echo substr(strip_tags($news->content_post),0,100); ?></p>
                                    </div>
                                    <a href="<?php echo URL . $this->menus['lang'] . '/news/post/'.$news->url_post ?>" class="link"></a>
                                </article>
                            </div>
                            <?php
                        }?>
                        <div class="col-md-12">
                            <div class="text text-center padding-bottom-null padding-md-top-null">
                                <a href="<?php echo URL . $this->menus['lang'] . '/news' ?>"
                                   class="btn-alt active shadow small margin-null"><?php echo ($this->menus['lang'] == 'id'?'Selebihnya':'More')?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="row margin-leftright-null">
            <div class="dark-background">
                <div class="col-md-12 clearfix dark-background padding-leftright-null">
                    <div class="partners z-index owl-carousel-logo">
                        <div class="partner">
                            <img class="img-responsive home" src="<?php echo URL ?>public/images/avava_da_jia.jpg"
                                 alt="">
                        </div>
                        <div class="partner">
                            <img class="img-responsive home" src="<?php echo URL ?>public/images/avava_hotel.jpg"
                                 alt="">
                        </div>
                        <div class="partner">
                            <img class="img-responsive home" src="<?php echo URL ?>public/images/avava_karaoke.jpg"
                                 alt="">
                        </div>
                        <div class="partner">
                            <img class="img-responsive home" src="<?php echo URL ?>public/images/avava_mall.jpg" alt="">
                        </div>
                        <div class="partner">
                            <img class="img-responsive home" src="<?php echo URL ?>public/images/avava_mart.jpg" alt="">
                        </div>
                        <div class="partner">
                            <img class="img-responsive home" src="<?php echo URL ?>public/images/avava_plaza.jpg"
                                 alt="">
                        </div>
                        <div class="partner">
                            <img class="img-responsive home" src="<?php echo URL ?>public/images/avava_school.jpg"
                                 alt="">
                        </div>
                        <div class="partner">
                            <img class="img-responsive home" src="<?php echo URL ?>public/images/avava_tanjung.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>