<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 17/11/2016
 * Time: 11:42
 */ ?>
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="kontak fullpage-wrap small">
        <ul class="slides">
            <li style="background-image: url(<?php echo $this->news[0]->image_post; ?>); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small"><?php echo $this->news[0]->title_post; ?></h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="post-wrap" class="content-section fullpage-wrap">
        <div class="row padding-onlytop-md content-post no-margin">
            <div class="col-md-offset-3 col-md-6 padding-leftright-null">
                <h2><?php echo $this->news[0]->title_post; ?></h2>
                <span class="date"><?php echo $this->news[0]->created_post; ?></span>
            </div>
            <div class="col-md-offset-3 col-md-6 padding-leftright-null padding-onlytop-md">
                <?php echo $this->news[0]->content_post; ?>
            </div>

        </div>
        <!--  Post Meta  -->
        <div class="row no-margin">
            <div class="col-md-offset-3 col-md-6 padding-leftright-null">
                <div id="post-meta">
                </div>
            </div>
        </div>
        <!--  END Post Meta  -->
        <div id="share">
            <a class="share-btn">
                <i class="material-icons">share</i>
            </a>
            <div class="share-icons" style="display:none">
                <a href="" class="share-google">
                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                </a>
                <a href="" class="share-twitter">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                <a href="" class="share-linkedin">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                </a>
                <a href="" class="share-mail">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <!--  Comments  -->
        <div class="row no-margin">
            <div class="col-md-offset-3 col-md-6 padding-leftright-null">
                <div id="comments">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab-one" aria-controls="tab-one" role="tab"
                                                                  data-toggle="tab" aria-expanded="true">All
                                comments</a></li>
                        <li role="presentation" class=""><a href="#tab-two" aria-controls="tab-two" role="tab"
                                                            data-toggle="tab" aria-expanded="false">Leave a comment</a>
                        </li>
                    </ul>
                    <!--  Nav Tabs  -->
                    <!-- Tab panes -->
                    <div class="tab-content no-margin-bottom">
                        <div role="tabpanel" class="tab-pane padding-md active" id="tab-one">
                            <?php
                            foreach ($this->comments as $comment) {
                                ?>

                                <div class="comment">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-2">
                                            <img src="<?php echo URL .'public/images/user.png'?>" height="60" width="60" alt="">
                                        </div>
                                        <div class="col-xs-9 col-sm-10">
                                            <h3>
                                                        <span class="comment-author">
                                                            <?php echo $comment->name_comments;?>
                                                        </span>
                                                <span class="comment-date">

                                                            <?php echo $comment->created_comments;?>
                                                        </span>
                                            </h3>
                                            <p>
                                                <?php echo $comment->content_comments;?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div role="tabpanel" class="tab-pane padding-md" id="tab-two">
                            <section class="comment-form">
                                <form id="contact-form" method="post"
                                      action="<?php echo URL . $this->menus['lang'] . '/news/comment/' . $this->news[0]->id_post; ?>">

                                    <input class="form-field" name="url" id="name" type="text"
                                           value="<?php echo $this->news[0]->url_post; ?>"
                                           placeholder="Name" hidden>
                                    <div class="row" >

                                        <div class="col-md-6">
                                            <input class="form-field" name="name_comments" id="name" type="text"
                                                   placeholder="Name" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-field" name="email_comments" id="mail" type="email"
                                                   placeholder="Email" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <textarea class="form-field" name="content_comments" id="messageForm"
                                                      rows="6"
                                                      placeholder="Your Message" required></textarea>
                                            <div class="submit-area">
                                                <input type="submit" id="submit-contact" class="btn-alt" value="Submit">
                                                <div id="msg" class="message"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  END Comments  -->
        <!--  Related News  -->
        <div id="related-news" class="row margin-leftright-null grey-background">
            <div class="col-md-12 padding-leftright-null">
                <div class="text padding-bottom-null text-center">
                    <h2 class="title center grey margin-bottom-small">Berita Lainnya</h2>
                </div>
            </div>
            <? ?>
            <div class="col-md-12 text padding-top-null related-news" id="news">
                <?php
                foreach ($this->beforeafterNews->data as $news) {
                    ?>
                    <div class="col-sm-6 single-news horizontal-news">
                        <article class="sleep">
                            <div class="col-md-6 padding-leftright-null">
                                <div class="image"
                                     style="background-image:url(<?php echo $news[0]->image_post; ?>)"></div>
                            </div>
                            <div class="col-md-6 padding-leftright-null">
                                <div class="content">
                                            <span class="read">
                                                Read More
                                            </span>
                                    <h3><?php echo $news[0]->title_post; ?>
                                    </h3>
                                    <span class="date"><?php echo $news[0]->created_post; ?></span>
                                    <p><?php echo substr(strip_tags($news[0]->content_post), 0, 100); ?></p>
                                </div>
                            </div>
                            <a href="<?php echo URL . $this->menus['lang'] . '/news/post/' . $news[0]->url_post ?>"
                               class="link"></a>
                        </article>
                    </div>
                    <?php
                } ?>
            </div>
        </div>
        <!--  END Related News  -->
    </div>
</div>
