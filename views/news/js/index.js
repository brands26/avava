/**
 * Created by Brands-PC on 22/11/2016.
 */
$(document).on("click","#button-search",function() {
    var key =$('#search-input').val();
    var link =$('#search-input').attr('data-href');
    window.location.href =link+key;
});
$('#search-form').submit(function(e){
    return false;
});
$('#search-input').keyup(function(e){
    e.preventDefault();
    var key =$(this).val();
    var link =$(this).attr('data-href');
    if(e.keyCode == 13)
    {
        window.location.href =link+key;
    }
});