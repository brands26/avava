<?php
/**
 * Created by PhpStorm.
 * User: Brands
 * Date: 11/18/2016
 * Time: 10:33 PM
 */
?>
<!--  Page Content, class footer-fixed if footer is fixed  -->
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <?php if (isset($this->group[0]->caraousel_image_group)) { ?>
        <div id="flexslider-nav" class="h450 fullpage-wrap small">

            <div class="slider-navigation">
                <a href="#" class="flex-prev"><i class="material-icons">keyboard_arrow_left</i></a>
                <div class="slider-controls-container"></div>
                <a href="#" class="flex-next"><i class="material-icons">keyboard_arrow_right</i></a>
            </div>
            <ul class="slides">
                <?php
                $datas = explode(',', $this->group[0]->caraousel_image_group);
                foreach ($datas as $data) {
                    ?>
                    <li style="background-image: url(<?php echo URL . 'public/images/uploads/'.$data ?>);"
                        class="clone" aria-hidden="true">
                        <div class="text">
                            <h1 class="white flex-animation no-opacity animated fadeInUp"><?php echo $this->group[0]->name_group; ?></h1>
                        </div>
                        <div class="gradient dark"></div>
                    </li>
                    <?php
                }

                ?>
            </ul>
        </div>
    <?php } else {
        ?>
            <div class="padding-onlytop-lg"></div>
            <div class="padding-onlytop-lg"></div>
            <div class="padding-onlytop-lg"></div>
            <div class="padding-onlytop-lg"></div>
        <?php
    } ?>
    <!--  END Slider  -->
    <?php if (isset($this->group[0]->content_group)) {
        ?>

        <div id="home-wrap" class="content-section fullpage-wrap">
            <div class="row margin-leftright-null text-center">
                <div class="col-md-12 padding-leftright-null">
                    <div class="text">
                        <div class="heading padding-onlytop-sm heading center">
                            <?php echo ($this->menus['lang'] == 'id'? ($this->group[0]->content_group != null?$this->group[0]->content_group:$this->group[0]->content_group_en):($this->group[0]->content_group_en != null?$this->group[0]->content_group_en:$this->group[0]->content_group)); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } ?>
</div>
