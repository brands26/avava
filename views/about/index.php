<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 17/11/2016
 * Time: 11:13
 */
?>
<!--  Page Content, class footer-fixed if footer is fixed  -->
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="kontak fullpage-wrap small">

        <div class="slider-navigation">
            <a href="#" class="flex-prev"><i class="material-icons">keyboard_arrow_left</i></a>
            <div class="slider-controls-container"></div>
            <a href="#" class="flex-next"><i class="material-icons">keyboard_arrow_right</i></a>
        </div>
        <ul class="slides">
            <li style="background-image: url(<?php echo URL.'public/images/uploads/'.$this->page[0]->image_page; ?>); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="kontak flex-active-slide">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small white flex-animation no-opacity animated fadeInUp"><?php echo($this->menus['lang'] == 'id' ? $this->page[0]->title_page : $this->page[0]->title_page_en) ?></h1>
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <div class="row margin-leftright-null text-center">
            <div class="col-md-12 padding-leftright-null">
                <div class="text">
                    <div class="padding-onlytop-sm">
                        <?php echo($this->menus['lang'] == 'id' ? $this->page[0]->content_page : $this->page[0]->content_page_en) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>