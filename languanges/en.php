<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 17/11/2016
 * Time: 9:27
 */
class En{

    public function getMenu(){
        $menus = array();
        $menus['lang'] = "en";
        $menus['home'] = "Home";
        $menus['about'] = "About Avava";
        $menus['news'] = "News and Events";
        $menus['newsMenu'] = "News and Events";
        $menus['news'] = "News";
        $menus['event'] = "Events";
        $menus['foundation'] = "Foundation";
        $menus['career'] = "Careers";
        $menus['contact'] = "Contact Us";
        return $menus;
    }

}
?>