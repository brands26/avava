<?php
/**
 * Created by PhpStorm.
 * User: Brands-PC
 * Date: 17/11/2016
 * Time: 9:27
 */
class Id{
    public function getMenu(){
        $menus = array();
        $menus['lang'] = "id";
        $menus['home'] = "Beranda";
        $menus['about'] = "Tentang Avava";
        $menus['newsMenu'] = "Berita dan Acara";
        $menus['news'] = "Berita";
        $menus['event'] = "Acara";
        $menus['foundation'] = "Foundation";
        $menus['career'] = "Karir";
        $menus['contact'] = "Kontak kami";
        return $menus;
    }

}
?>